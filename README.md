# Controls
Controls per teclat:

| Tecles | Acció |
| ------ | ------ |
|🡠/🡡/🡣/🡢      |`Moure el personatge pel món / Moure's per les opcions dels menus`  |
|SPACE / ENTER   |`Confirmar / Acceptar`            |
|ESC             |`Cancel·lar`            |
|A               |`Acceptar a quin enemic s'ataca`|
|E               |`Parlar amb el NPC de la botiga / Dormir en el llit`|

# Gameplay

En el menú principal tens l'opció de crear una nova partida o de continuar amb la partida que hagis guardat anteriorment.

<img src="RPG/GitScreens/img2.PNG" width="500">

Hauràs d'explorar diferents escenaris: per exemple una cova, un bosc, un castell abandonat, el castell final...

<img src="RPG/GitScreens/img3.PNG" width="500">

En aquestes dungeon en trobaràs amb diversos enemics que hauràs de derrotar per poder avançar.

<img src="RPG/GitScreens/img13.PNG" width="500">

Al derrotar aquests enemics, aconseguiràs experiència que faran pujar les estadístiques dels teus personatges.

<img src="RPG/GitScreens/img14.PNG" width="500">

Al pujar de nivell, els teus stats aniran incrementant poc a poc. Cada cop costarà més de pujar de nivell. Al pujar de nivell, els personatges obtindran nivells superiors de les seves Skill/Limit, que són atacs més forts que gasten MP. Poden pujar fins al nivell III.

<img src="RPG/GitScreens/img11.PNG" width="500">

Hi ha 6 diferents tipus de Skill:
- Atac: skill ofensiva
- Buff Atac: buff en l'atac en combat a un o més aliats 
- Buff Defensa: buff en la defensa en combat a un o més aliats 
- Curar: skill que cura a un o més aliats
- Debuff Atac: debuff a l'atac a un o més enemics
- Debuff Defensa: debuff a la defensa a un o més enemics

Tots els tipus de Skill poden anar dirigides a un o més target, dependrà del seu nivell principalment.

Hi ha dues botigues en el món, ubicades dins de cada poble.

<img src="RPG/GitScreens/img4.PNG" width="500">

En aquestes botigues podràs restablir el teu inventari. Podràs comprar armes, que les podràs equipar als teus personatges per pujar els seus stats globals.

<img src="RPG/GitScreens/img6.PNG" width="500">

També podràs comprar accessoris els quals també pujaran les estadístiques dels teus personatges.
globals. En cada botiga hi ha armes i accessoris diferents. Si no trobes l'arma perfecta pel teu personatge, pots probar sort en l'altra botiga.

<img src="RPG/GitScreens/img7.PNG" width="500">

A més pots comprar pocions que podràs utilitzar en combat per curar els teus personatges per exemple.

<img src="RPG/GitScreens/img8.PNG" width="500">

L'opció de reclutar et permet comprar nous personatges. En total hi ha 10 personatges jugables, cadascú amb una classe única i skills diferents. Cada cop et serà més car reclutar personatges.

<img src="RPG/GitScreens/img5.PNG" width="500">

Abans d'abandonar la botiga, pots dormir i recuperar forces a tot l'equip.

<img src="RPG/GitScreens/img4.PNG" width="500">

En el menú principal podràs personalitzar l'equipació dels teus personatges amb les armes/accessoris que hagis adquirit. També pots fer altres coses com curar els teus personatges amb pocions, organitzar el teu team principal amb tots els personatges que hagis reclutat, veure les seves estadístiques, guardar partida...

<img src="RPG/GitScreens/img10.PNG" width="500">


## Ampliacions

- Pathfinding en els enemics de l'overworld
- 8 escenaris diferents: Overworld, Cova, Bosc (fons amb parallax), Castell Abandonat, Castell Final, Poble 1, Poble 2, Botiga
- Els NPC dels pobles tenen diàlegs.
- 10 personatges diferents:
> Nivell incremental. Al pujar l'experiència pugen les seves stats automàticament. Cada personatge té stats diferents.

> Skills pròpies. Cada personatge té dos tipus de Skill, la normal i la limit. Cadascuna té 3 nivells diferents. Els nivells d'aquestes skills pugen al pujar de nivell als personatges. Gasten MP.

> Els personatges poden equipar 1 arma i 2 accessoris. Al equipar aquests objectes incrementaran els seus stats.

> Cada personatge té les seves pròpies animacions i efectes de veu.

- Menús contextuals complexos:
> Utilitzar ítems de curació

> Equipar armes/accessoris

> Organitzar la formació de la teva Party

> Menús contextuals de compra en les botigues, etc

- Els enemics ataques automàticament en combat. Al guanyar el combat, adquireixes XP i diners depenent del nivell dels enemics. Els nivells dels enemics i els seus stats es calculen automàticament en funció del nivell dels teus personatges per aconseguir una dificultat dinàmica.

# Crèdits

_Alberto Rivera Barrero_
* **Gitlab**: [ajrivera](https://gitlab.com/ajrivera)

_Daniel García Hernández_
* **Gitlab**: [dabrelad](https://gitlab.com/dabrelad)

_Adrià Gironès Calzada_
* **Gitlab**: [adriagirones](https://gitlab.com/adriagirones)
