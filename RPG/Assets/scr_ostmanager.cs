﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scr_ostmanager : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioSource start;
    public AudioSource overworld;
    public AudioSource fight;
    public AudioSource cave;
    public AudioSource forest;
    public AudioSource castle1;
    public AudioSource castle2;
    public AudioSource village1;
    public AudioSource village2;

    string scenename = "";

    private void Awake()
    {
        scenename = SceneManager.GetActiveScene().name;
        start = start.GetComponent<AudioSource>();
        overworld = overworld.GetComponent<AudioSource>();
    }

    void Start()
    {
        switch (scenename)
        {
            case "Start":
                start.Play();
                break;
            case "DaniScene":
                overworld.Play();
                break;
            case "Fight":
                fight.Play();
                break;
            case "Cave":
                cave.Play();
                break;
            case "Forest":
                forest.Play();
                break;
            case "ForestBoss":
                forest.Play();
                break;
            case "Castle1":
                castle1.Play();
                break;
            case "Castle2":
                castle2.Play();
                break;
            case "Village1":
                village1.Play();
                break;
            case "Town2":
                village2.Play();
                break;
            case "Inn":
                village1.Play();
                break;
            case "Inn2":
                village2.Play();
                break;
            default:
                overworld.Play();
                break;

        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
