﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scr_Character : MonoBehaviour
{
    public so_Character stats;
    //public so_Inventory inventory;

    private int actualAtk;
    private int actualDef;

    private so_BarHPEvent eventHP;
    private so_BarMPEvent eventMP;

    // Start is called before the first frame update
    void Start()
    {
        setActualAtk(stats.atk);
        setActualDef(stats.def);
        //print(getActualAtk());

        //PROVISIONAL
        //stats.actualWeapon = weapons[0];
        //stats.object1 = objects[0];
        // stats.object2 = objects[1];

        //stats.actualWeapon = inventory.weapons[0];
        //stats.object1 = inventory.equipableObjects[0];
        //stats.object2 = inventory.equipableObjects[1];
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            stats.upExp(100);
        }
        print(stats.name+"/"+stats.lvl+"/"+stats.xp+"/"+stats.xpNextLevel);
    }

    public void setHPMPEvent(so_BarHPEvent _event, so_BarMPEvent _eventmp)
    {
        eventHP = _event;
        eventMP = _eventmp;
    }

    public void updateHPMPEvent()
    {
        if (eventHP != null)
        {
            eventHP.RaiseItem(this.gameObject);
        }
        if(eventMP != null)
        {
            eventMP.RaiseItem(this.gameObject);
        }
    }

    public void setActualAtk(int _actualAtk)
    {
        actualAtk = _actualAtk;
    }

    public int getActualAtk()
    {
        return actualAtk;
    }

    public void setActualDef(int _actualDef)
    {
        actualDef = _actualDef;
    }

    public int getActualDef()
    {
        return actualDef;
    }

    public so_Weapon getActualWeapon()
    {
         return stats.actualWeapon;
    }

    public so_EquipmentAccessory getActualEquipableItem1()
    {
        return stats.object1;
    }

    public so_EquipmentAccessory getActualEquipableItem2()
    {
        return stats.object2;
    }

    public bool isDead()
    {
        if (this.stats.hp == 0)
        {
            return true;
        }
        else
            return false;
    }

    public int Attack()
    {
        int damage = getActualAtk();
        if(stats.actualWeapon != null)
        {
            damage += stats.actualWeapon.atk;
        }
        if (stats.object1 != null)
        {
            damage += stats.object1.atk;
        }
        if (stats.object2 != null)
        {
            damage += stats.object2.atk;
        }
        return damage;

    }

    public void Hurt(int _damage)
    {
        int def = getActualDef();
        if (stats.object1 != null)
        {
            def += stats.object1.def;
        }
        if (stats.object2 != null)
        {
            def += stats.object2.def;
        }

        if(_damage - def * 0.5f > 0) //TO DO: HACER UNA FÓRMULA DECENTE ¿?
        {
            stats.hp -= _damage - (int) (def * 0.5f);
            if (stats.hp < 0)
                stats.hp = 0;
        }
        if (eventHP != null)
        {
            eventHP.RaiseItem(this.gameObject);
        }
    }

    private void OnDisable()
    {
        EditorUtility.SetDirty(stats);
    }

    public void BuffAttack(int buff)
    {
        actualAtk += buff;
    }

    public void BuffDefense(int buff)
    {
        actualDef += buff;
    }

    public void Heal(int hp)
    {
        stats.hp += hp;
        if(stats.hp > stats.getGlobalMaxHP())
        {
            stats.hp = stats.getGlobalMaxHP();
        }
        if (eventHP != null)
        {
            eventHP.RaiseItem(this.gameObject);
        }
    }

    public int Skill()
    {
        if (eventMP != null)
        {
            eventMP.RaiseItem(this.gameObject);
        }
        switch (stats.actualSkill().type)
        {
            case so_Skills.TypeSkill.ATACK:
                return Attack() + stats.actualSkill().valor;

            default:
                return stats.actualSkill().valor;
        }   
    }

    public void Skill(List<GameObject> _party, List<GameObject> _enemies)
    {
        if (eventMP != null)
        {
            eventMP.RaiseItem(this.gameObject);
        }
        switch (stats.actualSkill().type)
        {
            case so_Skills.TypeSkill.ATACK:
                foreach(GameObject enemy in _enemies)
                {
                    enemy.GetComponent<scr_Enemy>().Hurt(Attack()+stats.actualSkill().valor);
                }
                break;
            case so_Skills.TypeSkill.DEBUFF_ATK:
                foreach (GameObject enemy in _enemies)
                {
                    enemy.GetComponent<scr_Enemy>().debuffAtk(stats.actualSkill().valor);
                }
                break;
            case so_Skills.TypeSkill.DEBUFF_DEF:
                foreach (GameObject enemy in _enemies)
                {
                    enemy.GetComponent<scr_Enemy>().debuffDef(stats.actualSkill().valor);
                }
                break;
            case so_Skills.TypeSkill.BUFF_ATK:
                foreach (GameObject character in _party)
                {
                    character.GetComponent<scr_Character>().BuffAttack(stats.actualSkill().valor);
                }
                break;
            case so_Skills.TypeSkill.BUFF_DEF:
                foreach (GameObject character in _party)
                {
                    character.GetComponent<scr_Character>().BuffDefense(stats.actualSkill().valor);
                }
                break;
            case so_Skills.TypeSkill.HEAL:
                foreach (GameObject character in _party)
                {
                    character.GetComponent<scr_Character>().Heal(stats.actualSkill().valor);
                }
                break;
        }
    }

    public int Limit()
    {
        if (eventMP != null)
        {
            eventMP.RaiseItem(this.gameObject);
        }
        switch (stats.actualLimit().type)
        {
            case so_Skills.TypeSkill.ATACK:
                return Attack() + stats.actualLimit().valor;

            default:
                return stats.actualLimit().valor;
        }
    }

    public void Limit(List<GameObject> _party, List<GameObject> _enemies)
    {
        if (eventMP != null)
        {
            eventMP.RaiseItem(this.gameObject);
        }
        switch (stats.actualLimit().type)
        {
            case so_Skills.TypeSkill.ATACK:
                foreach (GameObject enemy in _enemies)
                {
                    enemy.GetComponent<scr_Enemy>().Hurt(Attack() + stats.actualLimit().valor);
                }
                break;
            case so_Skills.TypeSkill.DEBUFF_ATK:
                foreach (GameObject enemy in _enemies)
                {
                    enemy.GetComponent<scr_Enemy>().debuffAtk(stats.actualLimit().valor);
                }
                break;
            case so_Skills.TypeSkill.DEBUFF_DEF:
                foreach (GameObject enemy in _enemies)
                {
                    enemy.GetComponent<scr_Enemy>().debuffDef(stats.actualLimit().valor);
                }
                break;
            case so_Skills.TypeSkill.BUFF_ATK:
                foreach (GameObject character in _party)
                {
                    character.GetComponent<scr_Character>().BuffAttack(stats.actualLimit().valor);
                }
                break;
            case so_Skills.TypeSkill.BUFF_DEF:
                foreach (GameObject character in _party)
                {
                    character.GetComponent<scr_Character>().BuffDefense(stats.actualLimit().valor);
                }
                break;
            case so_Skills.TypeSkill.HEAL:
                foreach (GameObject character in _party)
                {
                    character.GetComponent<scr_Character>().Heal(stats.actualLimit().valor);
                }
                break;
        }
    }
}
