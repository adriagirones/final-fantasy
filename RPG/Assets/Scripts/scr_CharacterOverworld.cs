﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scr_CharacterOverworld : MonoBehaviour
{
    Rigidbody2D rb;
    public float vel;
    public GameObject gameCamera;

    public delegate void updateTarget();
    public event updateTarget updateTargetEvent;

    public delegate void changeScene(string scene);
    public event changeScene changeSceneEvent;

    public so_OverworldPJ persistenceTransform;
    public so_PersistenceFight persistenceFight;
    bool hasExitFirstColl;
    float initStartX;
    float initStartY;
    int acumExitColl;
    Animator anim;

    public bool canMoveCameraVertical;


    public GameObject ContextMenu;

    enum direction { UP, DOWN, LEFT, RIGHT};
    direction charDirec;

    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
        this.gameObject.GetComponent<Transform>().position = persistenceTransform.PJtransform;
        initStartX = this.gameObject.transform.position.x;
        initStartY = this.gameObject.transform.position.y;
        acumExitColl = 0;
        hasExitFirstColl = false;
        anim = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!ContextMenu.activeSelf && this.gameObject.GetComponent<Rigidbody2D>().constraints != RigidbodyConstraints2D.FreezeAll)
        {
            if (Input.GetKey(KeyCode.DownArrow))
            {
                rb.velocity = new Vector2(rb.velocity.x, -vel);
                charDirec = direction.DOWN;
                if (updateTargetEvent != null)
                {
                    updateTargetEvent.Invoke();
                }
            }
            else if (Input.GetKey(KeyCode.UpArrow))
            {
                rb.velocity = new Vector2(rb.velocity.x, vel);
                charDirec = direction.UP;
                if (updateTargetEvent != null)
                {
                    updateTargetEvent.Invoke();
                }
            }
            else
            {
                rb.velocity = new Vector2(rb.velocity.x, 0);
            }

            if (Input.GetKey(KeyCode.RightArrow))
            {
                rb.velocity = new Vector2(vel, rb.velocity.y);
                charDirec = direction.RIGHT;
                if (updateTargetEvent != null)
                {
                    updateTargetEvent.Invoke();
                }
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                rb.velocity = new Vector2(-vel, rb.velocity.y);
                charDirec = direction.LEFT;
                if (updateTargetEvent != null)
                {
                    updateTargetEvent.Invoke();
                }
            }
            else
            {
                rb.velocity = new Vector2(0, rb.velocity.y);
            }
            setAnimation();


            if (this.gameObject.transform.position.x != initStartX || this.gameObject.transform.position.y != initStartY)
            {
                acumExitColl++;
                initStartX = this.gameObject.transform.position.x;
                initStartY = this.gameObject.transform.position.y;
                if (acumExitColl == 10)
                {
                    hasExitFirstColl = true;
                }
            }
            if (canMoveCameraVertical)
                gameCamera.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, gameCamera.transform.position.z);
            else
                gameCamera.transform.position = new Vector3(this.transform.position.x, gameCamera.transform.position.y, gameCamera.transform.position.z);
        }
    }

    private void setAnimation()
    {
        
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            //print("down");
                anim.SetInteger("StateMachine", 1);
            //anim.SetBool("Down", true);
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            //print("up");
                anim.SetInteger("StateMachine", 2);
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            //print("right");
                anim.SetInteger("StateMachine", 3);
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            //print("left");
                anim.SetInteger("StateMachine", 4);
        }
        
        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            //print("downII");
            anim.SetInteger("StateMachine", 5);
        }
        else if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            //print("upII");
            anim.SetInteger("StateMachine", 6);
        }
        else if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            //print("rightII");
            anim.SetInteger("StateMachine", 7);
        }
        else if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            //print("leftII");
            anim.SetInteger("StateMachine", 8);
        }

        /*
        if (rb.velocity.x != 0)
        {
            if(rb.velocity.x > 0)
            {
                if(anim.GetInteger("StateMachine") != 3)
                    
            }
            else 
            {
                if (anim.GetInteger("StateMachine") != 4)
                    anim.SetInteger("StateMachine", 4);
            }
               
        }
        else if(rb.velocity.y != 0) {
            if (rb.velocity.y > 0)
            {
                if (anim.GetInteger("StateMachine") != 2)
                    anim.SetInteger("StateMachine", 2);
            }
            else 
            {
                if (anim.GetInteger("StateMachine") != 1)
                    anim.SetInteger("StateMachine", 1);
            }       
        }
        else
        {
            switch (charDirec)
            {
                case direction.DOWN:
                    anim.SetInteger("StateMachine", 5);
                    break;
                case direction.UP:
                    anim.SetInteger("StateMachine", 6);
                    break;
                case direction.RIGHT:
                    anim.SetInteger("StateMachine", 7);
                    break;
                case direction.LEFT:
                    anim.SetInteger("StateMachine", 8);
                    break;
            }
        }*/

    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "LoadableScene")
        {
            if (hasExitFirstColl)
            {
                if (changeSceneEvent != null)
                {
                    persistenceTransform.PJtransform = this.gameObject.transform.position;
                    EditorUtility.SetDirty(persistenceTransform);
                    changeSceneEvent.Invoke(collision.gameObject.name);
                }
            }
        }

        if (collision.transform.tag == "Enemy")
        {
            persistenceTransform.PJtransform = this.gameObject.transform.position;
            persistenceFight.enemigo = collision.gameObject.name;
            persistenceFight.scene = SceneManager.GetActiveScene().name;
            EditorUtility.SetDirty(persistenceTransform);
            print(persistenceFight.enemigo);
            print(persistenceTransform.PJtransform);
            SceneManager.LoadScene("Fight");

        }

    }


}
