﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class scr_ShopManager : MonoBehaviour
{

    public so_Inventory shopInventory;
    public so_Inventory myInventory;
    public so_Team myTeam;
    public GameObject pj;

    public bool closed = true;
    /*
    enum menuphase
    {
        ChoosingItem,
        ChoosingChar

    }
    */

    public enum typeOfPurchase
    {
        POTIONS,
        WEAPONS,
        ACCESSORIES,
        RECRUITMENT
    }

    public so_UsableItemEvent healingItemEvent;

    public GameObject Icvs;
    public GameObject[] txtlist;
    private int index = 0;
    private bool indexchange = true;


    public GameObject page1cvs;
    public GameObject page2cvs;
    public GameObject page3cvs;
    private bool page2;
    private bool page3;

    public typeOfPurchase purchase;

    public TextMeshProUGUI moneyUI;

    public GameObject page4cvs;
    public GameObject[] txtChar;
    public GameObject recruitmentCost;

    // Start is called before the first frame update
    void Start()
    {
        var lista = shopInventory.recruitmentChar;
        for (var i = shopInventory.recruitmentChar.Count - 1; i >= 0; i--)
        {
            if (myTeam.Team.Contains(shopInventory.recruitmentChar[i]))
            {
                shopInventory.recruitmentChar.RemoveAt(i);
            }
        }
        updateInv();
    }

    private void updateInv()
    {
        //deactivate all
        for (int i = 0; i < txtlist.Length; i++)
        {
            txtlist[i].SetActive(false);
        }

        for (int i = 0; i < txtChar.Length; i++)
        {
            txtChar[i].SetActive(false);
        }

        //activate the ones we have
        if (purchase == typeOfPurchase.POTIONS)
        {
            print("usables");
            if (page2)
            {
                page2 = false;
                page2cvs.SetActive(false);
                page1cvs.SetActive(true);
            }

            for (int i = 0; i < shopInventory.usableItems.Count; i++)
            {
                if (txtlist.Length == i)
                    break;

                txtlist[i].SetActive(true);
                txtlist[i].GetComponent<TextMeshProUGUI>().text = shopInventory.usableItems[i].namePotion + "\n" + shopInventory.usableItems[i].description + "\nPrice - " + shopInventory.usableItems[i].price + " Q: " + shopInventory.usableItems[i].quantity;
                if (shopInventory.usableItems[i].sprite != null)
                {
                    //txtlist[i].transform.GetChild(0).GetComponent<Image>().color = Color.white;
                    //txtlist[i].transform.GetChild(0).GetComponent<Image>().sprite = shopInventory.usableItems[i].sprite;
                    txtlist[i].GetComponentInChildren<Image>().color = Color.white;
                    txtlist[i].GetComponentInChildren<Image>().sprite = shopInventory.usableItems[i].sprite;
                }
                else
                    txtlist[i].GetComponentInChildren<Image>().color = Color.clear;
            }

            if (shopInventory.usableItems.Count >= 21)
                page2 = true;
            else
                page2 = false;
            if (shopInventory.usableItems.Count >= 42)
                page3 = true;
            else
                page3 = false;
        }
        else if (purchase == typeOfPurchase.WEAPONS)
        {
            print("weapons");
            if (page2)
            {
                page2 = false;
                page2cvs.SetActive(false);
                page1cvs.SetActive(true);
            }
            for (int i = 0; i < shopInventory.weapons.Count; i++)
            {
                if (txtlist.Length == i)
                    break;

                txtlist[i].SetActive(true);
                txtlist[i].GetComponent<TextMeshProUGUI>().text = shopInventory.weapons[i].nameObject + "\n" + shopInventory.weapons[i].description + "\nPrice - " + shopInventory.weapons[i].price;
                if (shopInventory.weapons[i].sprite != null)
                {
                    txtlist[i].GetComponentInChildren<Image>().color = Color.white;
                    txtlist[i].GetComponentInChildren<Image>().sprite = shopInventory.weapons[i].sprite;
                }
                else
                    txtlist[i].GetComponentInChildren<Image>().color = Color.clear;
            }

            if (shopInventory.weapons.Count >= 21)
                page2 = true;
            else
                page2 = false;
            if (shopInventory.weapons.Count >= 42)
                page3 = true;
            else
                page3 = false;
        }
        else if(purchase == typeOfPurchase.ACCESSORIES)
        {
            if (page2)
            {
                page2 = false;
                page2cvs.SetActive(false);
                page1cvs.SetActive(true);
            }
            print("accesoriues");
            for (int i = 0; i < shopInventory.equipableObjects.Count; i++)
            {
                if (txtlist.Length == i)
                    break;

                txtlist[i].SetActive(true);
                txtlist[i].GetComponent<TextMeshProUGUI>().text = shopInventory.equipableObjects[i].nameObject + "\n"+ shopInventory.equipableObjects[i].description + "\nPrice - " + shopInventory.equipableObjects[i].price;
                if (shopInventory.equipableObjects[i].sprite != null)
                {
                    txtlist[i].GetComponentInChildren<Image>().color = Color.white;
                    txtlist[i].GetComponentInChildren<Image>().sprite = shopInventory.equipableObjects[i].sprite;
                }
                else
                    txtlist[i].GetComponentInChildren<Image>().color = Color.clear;
            }

            if (shopInventory.equipableObjects.Count >= 21)
                page2 = true;
            else
                page2 = false;
            if (shopInventory.equipableObjects.Count >= 42)
                page3 = true;
            else
                page3 = false;
        }
        else if (purchase == typeOfPurchase.RECRUITMENT)
        {
            page1cvs.SetActive(false);
            page4cvs.SetActive(true);
            recruitmentCost.SetActive(true);
            for (int i = 0; i < shopInventory.recruitmentChar.Count; i++)
            {
                if (txtChar.Length == i)
                    break;

                txtChar[i].SetActive(true);
                txtChar[i].GetComponent<TextMeshProUGUI>().text = shopInventory.recruitmentChar[i].GetComponent<scr_Character>().stats.nameCharacter + "\n" + shopInventory.recruitmentChar[i].GetComponent<scr_Character>().stats.type + "\n";
                txtChar[i].GetComponent<TextMeshProUGUI>().text += "HP:"+shopInventory.recruitmentChar[i].GetComponent<scr_Character>().stats.hp + " MP:" + shopInventory.recruitmentChar[i].GetComponent<scr_Character>().stats.mp + "\n";
                txtChar[i].GetComponent<TextMeshProUGUI>().text += "ATK:" + shopInventory.recruitmentChar[i].GetComponent<scr_Character>().stats.atk + " DEF:" + shopInventory.recruitmentChar[i].GetComponent<scr_Character>().stats.def;
                if (shopInventory.recruitmentChar[i].GetComponent<scr_Character>().stats.splashart != null)
                {
                    txtChar[i].GetComponentInChildren<Image>().color = Color.white;
                    txtChar[i].GetComponentInChildren<Image>().sprite = shopInventory.recruitmentChar[i].GetComponent<scr_Character>().stats.splashart;
                }
                else
                    txtChar[i].GetComponentInChildren<Image>().color = Color.clear;
            }
            recruitmentCost.GetComponentInChildren<TextMeshProUGUI>().text = "Recruitment cost: " + (myTeam.Team.Count * 1000);

        }

        moneyUI.text = "Money: "+myInventory.money;
    }

    // Update is called once per frame
    void Update()
    {
        if(purchase != typeOfPurchase.RECRUITMENT)
        {
            if (closed)
            {
                updateInv();
                closed = false;
            }
            //print(typeOfPurchase);
            //color red to choosed one
            if (indexchange)
                txtlist[index].GetComponent<TextMeshProUGUI>().color = Color.red;
            indexchange = false;

            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                if ((index + 1 % 7) != 0)
                {
                    if (txtlist[index + 1].activeSelf)
                    {
                        txtlist[index].GetComponent<TextMeshProUGUI>().color = Color.white;
                        index++;
                        indexchange = true;

                        if (index >= 42 && page3)
                        {
                            page2cvs.SetActive(false);
                            page3cvs.SetActive(true);
                        }
                        else if (index >= 21 && page2)
                        {
                            page1cvs.SetActive(false);
                            page2cvs.SetActive(true);
                        }
                    }
                }
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                if (index != 0)
                {
                    if (txtlist[index - 1].activeSelf)
                    {
                        txtlist[index].GetComponent<TextMeshProUGUI>().color = Color.white;
                        index--;
                        indexchange = true;

                        if (index < 21 && page2)
                        {
                            page2cvs.SetActive(false);
                            page1cvs.SetActive(true);
                        }
                        else if (index < 42 && page3)
                        {
                            page3cvs.SetActive(false);
                            page2cvs.SetActive(true);
                        }
                    }
                }
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                if (index < 55)
                {
                    if (txtlist[index + 7].activeSelf)
                    {
                        txtlist[index].GetComponent<TextMeshProUGUI>().color = Color.white;
                        index += 7;
                        indexchange = true;


                        if (index >= 42 && page3)
                        {
                            page2cvs.SetActive(false);
                            page3cvs.SetActive(true);
                        }
                        else if (index >= 21 && page2)
                        {
                            page1cvs.SetActive(false);
                            page2cvs.SetActive(true);
                        }
                    }
                }
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                if (index > 6)
                {
                    if (txtlist[index - 7].activeSelf)
                    {
                        txtlist[index].GetComponent<TextMeshProUGUI>().color = Color.white;
                        index -= 7;
                        indexchange = true;


                        if (index < 21 && page2)
                        {
                            page2cvs.SetActive(false);
                            page1cvs.SetActive(true);
                        }
                        else if (index < 42 && page3)
                        {
                            page3cvs.SetActive(false);
                            page2cvs.SetActive(true);
                        }
                    }

                }
            }
            else if (Input.GetKeyDown(KeyCode.Return))
            {
                if (purchase == typeOfPurchase.POTIONS)
                {
                    //COMPRAR SI HAY PASTA
                    if (myInventory.money >= shopInventory.usableItems[index].price)
                    {
                        if (!myInventory.usableItems.Contains(shopInventory.usableItems[index]))
                        {
                            myInventory.usableItems.Add(shopInventory.usableItems[index]);
                        }
                        else
                        {
                            myInventory.usableItems[myInventory.usableItems.IndexOf(shopInventory.usableItems[index])].quantity++;
                        }
                        myInventory.money -= shopInventory.usableItems[index].price;
                        updateInv();
                        print(myInventory.money);
                    }
                }
                else if (purchase == typeOfPurchase.WEAPONS)
                {
                    //COMPRAR SI HAY PASTA
                    if (shopInventory.weapons.Count > 0)
                    {
                        if (myInventory.money >= shopInventory.weapons[index].price)
                        {
                            myInventory.weapons.Add(shopInventory.weapons[index]);
                            myInventory.money -= shopInventory.weapons[index].price;
                            shopInventory.weapons.Remove(shopInventory.weapons[index]);
                            updateInv();
                            txtlist[index].GetComponent<TextMeshProUGUI>().color = Color.white;
                            if (index != 0)
                            {
                                index--;
                            }
                            indexchange = true;

                            print(myInventory.money);
                        }
                    }
                }
                else if (purchase == typeOfPurchase.ACCESSORIES)
                {
                    if (shopInventory.equipableObjects.Count > 0)
                    {
                        //COMPRAR SI HAY PASTA
                        if (myInventory.money >= shopInventory.equipableObjects[index].price)
                        {
                            myInventory.equipableObjects.Add(shopInventory.equipableObjects[index]);
                            myInventory.money -= shopInventory.equipableObjects[index].price;
                            shopInventory.equipableObjects.Remove(shopInventory.equipableObjects[index]);
                            updateInv();
                            txtlist[index].GetComponent<TextMeshProUGUI>().color = Color.white;
                            if (index != 0)
                            {
                                index--;
                            }
                            indexchange = true;

                            print(myInventory.money);
                        }
                    }
                }
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Icvs.SetActive(false);
                pj.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
                txtlist[index].GetComponent<TextMeshProUGUI>().color = Color.white;
                indexchange = true;
                index = 0;
            }
        }
        else
        {
            if (closed)
            {
                updateInv();
                closed = false;
            }

            if (indexchange)
                txtChar[index].GetComponent<TextMeshProUGUI>().color = Color.red;
            indexchange = false;

            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                if (index < 7)
                {
                    if (txtChar[index + 1].activeSelf)
                    {
                        txtChar[index].GetComponent<TextMeshProUGUI>().color = Color.white;
                        index++;
                        indexchange = true;
                    }
                }
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                if (index != 0)
                {
                    if (txtChar[index - 1].activeSelf)
                    {
                        txtChar[index].GetComponent<TextMeshProUGUI>().color = Color.white;
                        index--;
                        indexchange = true;
                    }
                }
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                if (index < 6)
                {
                    if (txtChar[index + 2].activeSelf)
                    {
                        txtChar[index].GetComponent<TextMeshProUGUI>().color = Color.white;
                        index += 2;
                        indexchange = true;
                    }
                }
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                if (index > 1)
                {
                    if (txtChar[index - 2].activeSelf)
                    {
                        txtChar[index].GetComponent<TextMeshProUGUI>().color = Color.white;
                        index -= 2;
                        indexchange = true;
                    }

                }
            }
            else if (Input.GetKeyDown(KeyCode.Return))
            {
                if (myInventory.money >= myTeam.Team.Count * 1000)
                {
                    myInventory.money -= myTeam.Team.Count * 1000;
                    myTeam.Team.Add(shopInventory.recruitmentChar[index]);
                    shopInventory.recruitmentChar.RemoveAt(index);
                    updateInv();
                    txtChar[index].GetComponent<TextMeshProUGUI>().color = Color.white;
                    if (index != 0)
                    {
                        index--;
                    }
                    indexchange = true;
                }
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                page1cvs.SetActive(true);
                page4cvs.SetActive(false);
                recruitmentCost.SetActive(false);
                Icvs.SetActive(false);
                pj.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
                txtChar[index].GetComponent<TextMeshProUGUI>().color = Color.white;
                indexchange = true;
                index = 0;
            }
        }
        

       
    }
    static int SortUsableItems(so_UsableItem u1, so_UsableItem u2)
    {
        return u1.quantity.CompareTo(u2.quantity);
    }

}
