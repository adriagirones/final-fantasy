﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_MusicManagerFight : MonoBehaviour
{
    public AudioSource[] a2;
    public AudioSource[] auron;
    public AudioSource[] domino;
    public AudioSource[] ishil;
    public AudioSource[] jade;
    public AudioSource[] malphasie;
    public AudioSource[] mercedes;
    public AudioSource[] roselia;
    public AudioSource[] tiz;
    public AudioSource[] victoria;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlaySFXAttack(string s)
    {
        int r = Random.Range(0, 2);
        switch (s)
        {
            case "A2":
                if (r == 0)
                    a2[0].Play();
                else
                    a2[1].Play();
                break;
            case "Auron":
                if (r == 0)
                    auron[0].Play();
                else
                    auron[1].Play();
                break;
            case "Domino":
                if (r == 0)
                    domino[0].Play();
                else
                    domino[1].Play();
                break;
            case "Ishil":
                if (r == 0)
                    ishil[0].Play();
                else
                    ishil[1].Play();
                break;
            case "Jade":
                if (r == 0)
                    jade[0].Play();
                else
                    jade[1].Play();
                break;
            case "Malphasie":
                if (r == 0)
                    malphasie[0].Play();
                else
                    malphasie[1].Play();
                break;
            case "Mercedes":
                if (r == 0)
                    mercedes[0].Play();
                else
                    mercedes[1].Play();
                break;
            case "Roselia":
                if (r == 0)
                    roselia[0].Play();
                else
                    roselia[1].Play();
                break;
            case "Tiz":
                if (r == 0)
                    tiz[0].Play();
                else
                    tiz[1].Play();
                break;
            case "Victoria":
                if (r == 0)
                    victoria[0].Play();
                else
                    victoria[1].Play();
                break;
        }
    }

    public void PlaySFXDamage(string s)
    {
        int r = Random.Range(0, 2);
        switch (s)
        {
            case "A2":
                if (r == 0)
                    a2[2].Play();
                else
                    a2[3].Play();
                break;
            case "Auron":
                if (r == 0)
                    auron[2].Play();
                else
                    auron[3].Play();
                break;
            case "Domino":
                if (r == 0)
                    domino[2].Play();
                else
                    domino[3].Play();
                break;
            case "Ishil":
                if (r == 0)
                    ishil[2].Play();
                else
                    ishil[3].Play();
                break;
            case "Jade":
                if (r == 0)
                    jade[2].Play();
                else
                    jade[3].Play();
                break;
            case "Malphasie":
                if (r == 0)
                    malphasie[2].Play();
                else
                    malphasie[3].Play();
                break;
            case "Mercedes":
                if (r == 0)
                    mercedes[2].Play();
                else
                    mercedes[3].Play();
                break;
            case "Roselia":
                if (r == 0)
                    roselia[2].Play();
                else
                    roselia[3].Play();
                break;
            case "Tiz":
                if (r == 0)
                    tiz[2].Play();
                else
                    tiz[3].Play();
                break;
            case "Victoria":
                if (r == 0)
                    victoria[2].Play();
                else
                    victoria[3].Play();
                break;
        }
    }

    public void PlaySFXDead(string s)
    {
        switch (s)
        {
            case "A2":
                a2[4].Play();
                break;
            case "Auron":
                auron[4].Play();
                break;
            case "Domino":
                domino[4].Play();
                break;
            case "Ishil":
                ishil[4].Play();
                break;
            case "Jade":
                jade[4].Play();
                break;
            case "Malphasie":
                malphasie[4].Play();
                break;
            case "Mercedes":
                mercedes[4].Play();
                break;
            case "Roselia":
                roselia[4].Play();
                break;
            case "Tiz":
                tiz[4].Play();
                break;
            case "Victoria":
                victoria[4].Play();
                break;
        }
    }

    public void PlaySFXStandBy(string s)
    {
        int r = Random.Range(0, 3);
        switch (s)
        {
            case "A2":
                if (r == 0)
                    a2[5].Play();
                else if (r == 1)
                    a2[6].Play();
                else
                    a2[7].Play();
                break;
            case "Auron":
                if (r == 0)
                    auron[5].Play();
                else if (r == 1)
                    auron[6].Play();
                else
                    auron[7].Play();
                break;
            case "Domino":
                if (r == 0)
                    domino[5].Play();
                else if (r == 1)
                    domino[6].Play();
                else
                    domino[7].Play();
                break;
            case "Ishil":
                if (r == 0)
                    ishil[5].Play();
                else if (r == 1)
                    ishil[6].Play();
                else
                    ishil[7].Play();
                break;
            case "Jade":
                if (r == 0)
                    jade[5].Play();
                else if (r == 1)
                    jade[6].Play();
                else
                    jade[7].Play();
                break;
            case "Malphasie":
                if (r == 0)
                    malphasie[5].Play();
                else if (r == 1)
                    malphasie[6].Play();
                else
                    malphasie[7].Play();
                break;
            case "Mercedes":
                if (r == 0)
                    mercedes[5].Play();
                 else if (r == 1)
                    mercedes[6].Play();
                else
                    mercedes[7].Play();
                break;
            case "Roselia":
                if (r == 0)
                    roselia[5].Play();
                 else if (r == 1)
                    roselia[6].Play();
                else
                    roselia[7].Play();
                break;
            case "Tiz":
                if (r == 0)
                    tiz[5].Play();
                else if (r == 1)
                    tiz[6].Play();
                else
                    tiz[7].Play();
                break;
            case "Victoria":
                if (r == 0)
                    victoria[5].Play();
                else if (r == 1)
                    victoria[6].Play();
                else
                    victoria[7].Play();
                break;
        }
    }

    public void PlaySFXWin(string s)
    {
        switch (s)
        {
            case "A2":
                a2[8].Play();
                break;
            case "Auron":
                auron[8].Play();
                break;
            case "Domino":
                domino[8].Play();
                break;
            case "Ishil":
                ishil[8].Play();
                break;
            case "Jade":
                jade[8].Play();
                break;
            case "Malphasie":
                malphasie[8].Play();
                break;
            case "Mercedes":
                mercedes[8].Play();
                break;
            case "Roselia":
                roselia[8].Play();
                break;
            case "Tiz":
                tiz[8].Play();
                break;
            case "Victoria":
                victoria[8].Play();
                break;
        }
    }

    public void PlaySFXSkill(string s)
    {
        int r = Random.Range(0, 2);
        switch (s)
        {
            case "A2":
                if (r == 0)
                    a2[9].Play();
                else
                    a2[10].Play();
                break;
            case "Auron":
                if (r == 0)
                    auron[9].Play();
                else
                    auron[10].Play();
                break;
            case "Domino":
                if (r == 0)
                    domino[9].Play();
                else
                    domino[10].Play();
                break;
            case "Ishil":
                if (r == 0)
                    ishil[9].Play();
                else
                    ishil[10].Play();
                break;
            case "Jade":
                if (r == 0)
                    jade[9].Play();
                else
                    jade[10].Play();
                break;
            case "Malphasie":
                if (r == 0)
                    malphasie[9].Play();
                else
                    malphasie[10].Play();
                break;
            case "Mercedes":
                if (r == 0)
                    mercedes[9].Play();
                else
                    mercedes[10].Play();
                break;
            case "Roselia":
                if (r == 0)
                    roselia[9].Play();
                else
                    roselia[10].Play();
                break;
            case "Tiz":
                if (r == 0)
                    tiz[9].Play();
                else
                    tiz[10].Play();
                break;
            case "Victoria":
                if (r == 0)
                    victoria[9].Play();
                else
                    victoria[10].Play();
                break;
        }
    }

    public void PlaySFXLimit(string s)
    {
        int r = Random.Range(0, 2);
        switch (s)
        {
            case "A2":
                if (r == 0)
                    a2[11].Play();
                else
                    a2[12].Play();
                break;
            case "Auron":
                if (r == 0)
                    auron[11].Play();
                else
                    auron[12].Play();
                break;
            case "Domino":
                if (r == 0)
                    domino[11].Play();
                else
                    domino[12].Play();
                break;
            case "Ishil":
                if (r == 0)
                    ishil[11].Play();
                else
                    ishil[12].Play();
                break;
            case "Jade":
                if (r == 0)
                    jade[11].Play();
                else
                    jade[12].Play();
                break;
            case "Malphasie":
                if (r == 0)
                    malphasie[11].Play();
                else
                    malphasie[12].Play();
                break;
            case "Mercedes":
                if (r == 0)
                    mercedes[11].Play();
                else
                    mercedes[12].Play();
                break;
            case "Roselia":
                if (r == 0)
                    roselia[11].Play();
                else
                    roselia[12].Play();
                break;
            case "Tiz":
                if (r == 0)
                    tiz[11].Play();
                else
                    tiz[12].Play();
                break;
            case "Victoria":
                if (r == 0)
                    victoria[11].Play();
                else
                    victoria[12].Play();
                break;
        }
    }

}
