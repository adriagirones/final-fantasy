﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scr_OverworldManager : MonoBehaviour
{
    public GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        player.GetComponent<scr_CharacterOverworld>().changeSceneEvent += OverworldLoadScene;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OverworldLoadScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }
   
}
