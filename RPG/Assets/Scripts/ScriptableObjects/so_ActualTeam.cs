﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_ActualTeam : ScriptableObject
{
    public GameObject[] myActualTeam;
}
