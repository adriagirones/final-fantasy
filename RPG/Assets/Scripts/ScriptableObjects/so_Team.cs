﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_Team : ScriptableObject
{
    public List<GameObject> Team;
}
