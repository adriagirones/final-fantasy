﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu]
public class so_Character : ScriptableObject
{
    public string nameCharacter;
    public int hp;
    public int maxhp;
    public int mp;
    public int maxmp;
    public int atk;
    public int def;
    public int xp;
    public int lvl;
    public int hpLvl1;
    public int mpLvl1;
    public int atkLvl1;
    public int defLvl1;
    public int xpNextLevel;

    public so_Weapon.TypeWeapon type;

    public so_Weapon actualWeapon;
    public so_EquipmentAccessory object1;
    public so_EquipmentAccessory object2;
    public so_Skills skill1;
    public so_Skills skill2;
    public so_Skills skill3;
    public so_Skills limitSkill1;
    public so_Skills limitSkill2;
    public so_Skills limitSkill3;
    public Sprite splashart;

    public so_Skills actualSkill()
    {
        if(lvl < 4)
        {
            return skill1;
        }
        else if(lvl < 7)
        {
            return skill2;
        }
        else
        {
            return skill3;
        }
    }

    public so_Skills actualLimit()
    {
        if (lvl < 4)
        {
            return limitSkill1;
        }
        else if (lvl < 7)
        {
            return limitSkill2;
        }
        else
        {
            return limitSkill3;
        }
    }



    public void upExp(int n)
    {
        if(lvl != 10)
        {
            xp += n;
            if (xp >= xpNextLevel)
            {
                int a = xp - xpNextLevel;
                xp = a;
                levelUp();
            }
        }
    }
    public void levelUp()
    {
        maxhp = (int)(hpLvl1 * ((lvl * 0.1f)+1));
        maxmp = (int)(mpLvl1 * ((lvl * 0.1f)+1));
        atk = (int)(atkLvl1 * ((lvl * 0.1f)+1));
        def = (int)(defLvl1 * ((lvl * 0.1f)+1));
        lvl++;
        if (lvl == 10)
        {
            xpNextLevel = 0;
            xp = 0;
        }
        else{
            xpNextLevel *= 2;
            hp = getGlobalMaxHP();
            mp = getGlobalMaxMP();
        }
    }

    public int getGlobalMaxHP(){
        int var = maxhp;
        if(actualWeapon != null)
            var += actualWeapon.hp;
        if (object1 != null)
            var += object1.hp;
        if (object2 != null)
            var += object2.hp;
        return var;
    }
    public int getGlobalMaxMP(){
        int var = maxmp;
        if(actualWeapon != null)
            var += actualWeapon.mp;
        if (object1 != null)
            var += object1.mp;
        if (object2 != null)
            var += object2.mp;
        return var;
    }
    public int getGlobalAtk(){
        int var = atk;
        if(actualWeapon != null)
            var += actualWeapon.atk;
        if (object1 != null)
            var += object1.atk;
        if (object2 != null)
            var += object2.atk;
        return var;
    }
    public int getGlobalDef(){
        int var = def;
        if(actualWeapon != null)
            var += actualWeapon.def;
        if (object1 != null)
            var += object1.def;
        if (object2 != null)
            var += object2.def;
        return var;
    }

    public void RestoreHP(so_HealingItem item)
    {
        hp += item.hp;
        if(hp > getGlobalMaxHP())
        {
            hp = getGlobalMaxHP();
        }
    }

    public void RestoreMP(so_RecoveryMPItem item)
    {
        mp += item.mp;
        if (mp > getGlobalMaxMP())
        {
            mp = getGlobalMaxMP();
        }
    }
}
