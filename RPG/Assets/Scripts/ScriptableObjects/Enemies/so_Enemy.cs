﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_Enemy : ScriptableObject
{
    public bool active = true;
    public int hp;
    public int hpMax;
    public int atk;
    public int def;
    public int xp;
}
