﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class so_UsableItem : ScriptableObject
{
    public string namePotion;
    public string description;
    public int quantity;
    public Sprite sprite;
    public int price;
}
