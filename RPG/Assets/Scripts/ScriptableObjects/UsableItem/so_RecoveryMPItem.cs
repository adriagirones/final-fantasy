﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_RecoveryMPItem : so_UsableItem
{
    public int mp;
}
