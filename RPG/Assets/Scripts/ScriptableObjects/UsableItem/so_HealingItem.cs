﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 [CreateAssetMenu]
public class so_HealingItem : so_UsableItem
{
    public int hp;
}
