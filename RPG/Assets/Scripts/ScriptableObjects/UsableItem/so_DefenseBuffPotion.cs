﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_DefenseBuffPotion : so_UsableItem
{
    public int def;
}
