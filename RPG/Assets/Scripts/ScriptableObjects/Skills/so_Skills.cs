﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_Skills : ScriptableObject 
{
    public string nameSkill;
    public string description;
    public int mpcost;
    public bool limitSkill;

    public enum TypeSkill
    {
        ATACK,
        BUFF_ATK,
        BUFF_DEF,
        DEBUFF_ATK,
        DEBUFF_DEF,
        HEAL
    }

    public enum Targets
    {
        ONE_TARGET,
        ALL_TARGETS
    }

    public TypeSkill type;
    public Targets numTargets;
    public int valor;

}
