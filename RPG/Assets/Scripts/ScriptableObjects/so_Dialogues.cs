﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_Dialogues : ScriptableObject
{   
    [SerializeField]
    public StringStringDictionary dialogues = StringStringDictionary.New<StringStringDictionary>();
    private Dictionary<string, string> stringString
    {
        get { return dialogues.dictionary; }
    }
}
