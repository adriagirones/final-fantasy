﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scr_BedManager : MonoBehaviour
{
    public GameObject PJ;
    public GameObject menuContext;
    public GameObject menu;
    public GameObject myCanvas;
    public so_Team team;
    public so_Inventory myInventory;
    public GameObject fade;

    private bool canSleep;
    private bool canInput;

    // Start is called before the first frame update
    void Start()
    {
        canSleep = false;
        canInput = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (canSleep && !menu.activeSelf && canInput)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                PJ.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
                myCanvas.SetActive(true);
                myCanvas.transform.GetChild(0).gameObject.SetActive(true);
                myCanvas.transform.GetChild(1).gameObject.SetActive(true);
                myCanvas.transform.GetChild(2).gameObject.SetActive(true);
                myCanvas.transform.GetChild(3).gameObject.SetActive(true);
                myCanvas.transform.GetChild(4).gameObject.SetActive(true);
                myCanvas.transform.GetChild(4).GetComponentInChildren<TMPro.TextMeshProUGUI>().text = "Money: " + myInventory.money;


                menuContext.SetActive(false);
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                PJ.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
                myCanvas.SetActive(false);
                menuContext.SetActive(true);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name.Equals("Character"))
        {
            canSleep = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.name.Equals("Character"))
        {
            canSleep = false;
        }
    }

    public void noOption()
    {
        PJ.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
        myCanvas.SetActive(false);
        menuContext.SetActive(true);
    }

    public void yesOption()
    {
        if(myInventory.money >= 200)
        {
            myInventory.money -= 200;
            foreach(GameObject go in team.Team)
            {
                go.GetComponent<scr_Character>().stats.hp = go.GetComponent<scr_Character>().stats.getGlobalMaxHP();
                go.GetComponent<scr_Character>().stats.mp = go.GetComponent<scr_Character>().stats.getGlobalMaxMP();
            }
            myCanvas.transform.GetChild(0).gameObject.SetActive(false);
            myCanvas.transform.GetChild(1).gameObject.SetActive(false);
            myCanvas.transform.GetChild(2).gameObject.SetActive(false);
            myCanvas.transform.GetChild(3).gameObject.SetActive(false);
            myCanvas.transform.GetChild(4).GetComponentInChildren<TMPro.TextMeshProUGUI>().text = "Money: " + myInventory.money;
            myCanvas.transform.GetChild(4).gameObject.SetActive(false);
            canInput = false;
            StartCoroutine(fadeBlack());
            //myCanvas.SetActive(false);
            //menuContext.SetActive(true);

        }

    }

    public IEnumerator fadeBlack(int fadeSpeed = 1)
    {
        Color objectColor = fade.GetComponent<Image>().color;
        float fadeAmount;

        while (fade.GetComponent<Image>().color.a < 1)
        {
            fadeAmount = objectColor.a + (fadeSpeed * Time.deltaTime);
            objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, fadeAmount);
            fade.GetComponent<Image>().color = objectColor;
            yield return null;
        }
        while (fade.GetComponent<Image>().color.a > 0)
        {
            fadeAmount = objectColor.a - (fadeSpeed * Time.deltaTime);
            objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, fadeAmount);
            fade.GetComponent<Image>().color = objectColor;
            yield return null;
        }
        myCanvas.SetActive(false);
        menuContext.SetActive(true);
        PJ.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
        canInput = true;
    }
}
