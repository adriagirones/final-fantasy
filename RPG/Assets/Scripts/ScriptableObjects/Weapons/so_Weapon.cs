﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_Weapon : so_EquipableObject
{
    public enum TypeWeapon
    {
        SWORD,
        AXE,
        LANCE,
        STAFF,
        SABLE,
        GREATSWORD,
        SUMMONER,
        KNUCKLES,
        BOW
    }

    public TypeWeapon type;
}
