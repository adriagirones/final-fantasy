﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class so_EquipableObject : ScriptableObject
{
    public string nameObject;
    public string description;
    public int hp;
    public int mp;
    public int atk;
    public int def;
    public bool equiped;
    public Sprite sprite;
    public int price;

}
