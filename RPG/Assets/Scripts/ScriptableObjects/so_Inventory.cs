﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_Inventory : ScriptableObject
{
    public List<so_UsableItem> usableItems = new List<so_UsableItem>();
    public List<so_Weapon> weapons = new List<so_Weapon>();
    public List<so_EquipmentAccessory> equipableObjects = new List<so_EquipmentAccessory>();
    public List<GameObject> recruitmentChar = new List<GameObject>();
    public int money = 0;
   // public LinkedList<so_UsableItem> usableItems = new LinkedList<so_UsableItem>();
   /*
    public void addUsableItem(so_UsableItem _object)
    {
         if (usableItems.ContainsKey(_object))
         {
             int val = usableItems[_object];
             usableItems.Remove(_object);
             usableItems.Add(_object, val + 1);
         }
         else
         {
             usableItems.Add(_object, 1);
         }
    }

    public void removeUsableItem(so_UsableItem _object)
    {
        if (usableItems.ContainsKey(_object))
        {
            usableItems.Remove(_object);
        }
    }

    public void addEquipableObject(so_EquipmentItem _object)
    {
        equipableObjects.AddLast(_object);
    }

    public void removeEquipableObject(so_EquipmentItem _object)
    {
        if (equipableObjects.Contains(_object))
        {
            equipableObjects.Remove(_object);
        }
    }

    public void addWeapon(so_Weapons _object)
    {
        weapons.AddLast(_object);
    }

    public void removeWeapon(so_Weapons _object)
    {
        weapons.AddLast(_object);
    }*/
}
