﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_PersistenceFight : ScriptableObject
{
    public String enemigo;
    public String scene;
}
