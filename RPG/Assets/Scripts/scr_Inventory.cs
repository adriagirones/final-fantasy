﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_Inventory : MonoBehaviour
{
    public so_Inventory inventory;
    //public so_UsableItem[] poolUsableItems;
   // public so_Weapon[] poolWeapons;
   // public so_EquipmentAccessory[] poolEquipmentItems;

    // Start is called before the first frame update
    void Awake()
    {
     //   inventory.weapons.Add(poolWeapons[0]);
      //  inventory.equipableObjects.Add(poolEquipmentItems[0]);
     //   inventory.equipableObjects.Add(poolEquipmentItems[1]);
        //inventory.usableItems.Add(poolUsableItems[0]);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    //REVISAR , cambiado de dictionary a list

    /*public void addUsableItem(so_UsableItem _object)
    {
        if (inventory.usableItems.ContainsKey(_object))
        {
            int val = inventory.usableItems[_object];
            inventory.usableItems.Remove(_object);
            inventory.usableItems.Add(_object, val+1);
        }
        else
        {
            inventory.usableItems.Add(_object, 1);
        }
    }

    public void removeUsableItem(so_UsableItem _object)
    {
        if (inventory.usableItems.ContainsKey(_object))
        {
            int val = inventory.usableItems[_object];
            if(val == 1)
            {
                inventory.usableItems.Remove(_object);
            }
            else
            {
                inventory.usableItems.Remove(_object);
                inventory.usableItems.Add(_object, val - 1);
            }
        }
    }
    */
    public void addEquipableObject(so_EquipmentAccessory _object)
    {
        inventory.equipableObjects.Add(_object);
    }

    public void removeEquipableObject(so_EquipmentAccessory _object)
    {
        if (inventory.equipableObjects.Contains(_object))
        {
            inventory.equipableObjects.Remove(_object);
        }
    }

    public void addWeapon(so_Weapon _object)
    {
        inventory.weapons.Add(_object);
    }

    public void removeWeapon(so_Weapon _object)
    {
        inventory.weapons.Add(_object);
    }
    
}
