﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class scr_Dialogues : MonoBehaviour
{

    public so_Dialogues dialogues;
    public GameObject textParent;
    public GameObject textBox;
    // Start is called before the first frame update
    void Start()
    {   
        if(textBox != null)
            textBox.SetActive(false);
        if(textParent != null)
         textParent.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.name.Equals("Character"))
        {
            if (dialogues.dialogues.dictionary.ContainsKey(this.gameObject.name))
            {
                textParent.SetActive(true);
                textBox.SetActive(true);
                print(this.gameObject.name);
                textBox.GetComponent<TMPro.TextMeshProUGUI>().SetText(dialogues.dialogues.dictionary[this.gameObject.name]);
            }
        }
    }


    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.name.Equals("Character"))
        {
            textParent.SetActive(false);
            textBox.SetActive(false);
        }
    }
}
