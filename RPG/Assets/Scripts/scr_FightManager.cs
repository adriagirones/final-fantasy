﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class scr_FightManager : MonoBehaviour
{
    public so_PersistenceFight persistence;

    public GameObject[] enemiesArray;
    private GameObject[] actualEnemies;
    public Vector3 enemyGeneralTransform;

    public so_ActualTeam myActualTeamSO;
    private GameObject[] myActualTeam;
    public Vector3 teamGeneralTransform;

    public GameObject canvasPlayerPhase;
    public GameObject debugFight;

    private List<GameObject> playersInFight = new List<GameObject>();
    private List<GameObject> enemiesInFight = new List<GameObject>();

    public GameObject prefabPointerEnemy;
    GameObject pointerEnemy;
    // private List<GameObject> pointerEnemies = new List<GameObject>();
    // public so_OverworldPJ sopj;

    private bool canAttackEnemy;

    public GameObject playerEventSystem;
    public GameObject magicDesc;
    public GameObject magic;
    public GameObject limitDesc;
    public GameObject limit;
    public TMPro.TextMeshProUGUI uiPhase;
    public GameObject results;
    public GameObject inventory;

    public so_BarHPEvent[] hpEventEnemy;
    public so_BarHPEvent[] hpEventPlayer;
    public so_BarMPEvent[] mpEventPlayer;

    public GameObject[] canvasStateEnemy;
    public GameObject[] canvasStatePlayer;

    public Sprite[] background;
    public GameObject backgroundObject;

    public scr_MusicManagerFight musicmanager;
    bool canPlay;

    public so_Inventory inv;
    
    public enum phaseType
    {
        ENEMY_TURN,
        PLAYER_TURN,
        PLAYER_ATTACK_CHOOSE_ENEMY,
        PLAYER_SKILL_CHOOSE_ENEMY,
        PLAYER_SKILL_CHOOSE_PLAYER,
        PLAYER_LIMIT_CHOOSE_ENEMY,
        PLAYER_LIMIT_CHOOSE_PLAYER,
        INVENTORY,
        WIN_TURN
    };

    phaseType actualPhase = phaseType.PLAYER_TURN;

    int torn;
    int actualTorn;
    int chooseEnemy;
    int choosePlayer;

    // Start is called before the first frame update
    void Start()
    {
        instanciarAliados();
        instanciarEnemigos();
        pointerEnemy = Instantiate(prefabPointerEnemy);
        pointerEnemy.SetActive(false);
        canAttackEnemy = false;
        switch(persistence.scene)
        {
            case "DaniScene":
                backgroundObject.GetComponent<SpriteRenderer>().sprite = background[0];
                break;
            case "Cave":
                backgroundObject.GetComponent<SpriteRenderer>().sprite = background[1];
                break;
            case "Forest":
                backgroundObject.GetComponent<SpriteRenderer>().sprite = background[2];
                break;
            case "ForestBoss":
                backgroundObject.GetComponent<SpriteRenderer>().sprite = background[3];
                break;
            case "Castle1":
                backgroundObject.GetComponent<SpriteRenderer>().sprite = background[4];
                break;
            case "Castle2":
                backgroundObject.GetComponent<SpriteRenderer>().sprite = background[5];
                break;
        }
        canPlay = true;

    }

    // Update is called once per frame
    void Update()
    {
        Debug();
        switch (actualPhase)
        {
            case phaseType.PLAYER_TURN:
                if (playersInFight[actualTorn].GetComponent<scr_Character>().isDead())
                {
                    canvasStatePlayer[actualTorn].GetComponent<Image>().color = new Color(0.65f, 0.65f, 0.65f, 0.8f);
                    actualTorn++;
                    canPlay = true;
                    if (actualTorn == playersInFight.Count)
                    {
                        actualTorn = 0;
                        actualPhase = phaseType.ENEMY_TURN;
                        uiPhase.text = "Enemy Phase";
                        StartCoroutine(delayPhaseEnemy());
                    }
                }
                else
                {
                    playersInFight[actualTorn].GetComponent<Animator>().SetInteger("StateMachine", 1);
                    if (canPlay)
                    {
                        StartCoroutine(delayPlaySFX());
                        canPlay = false;
                    }
                        
                    canvasPlayerPhase.SetActive(true);
                    canvasStatePlayer[actualTorn].GetComponent<Image>().color = new Color(1f, 0.33f, 0.33f, 0.8f);
                    if (playerEventSystem.GetComponent<EventSystem>().currentSelectedGameObject == magic)
                    {
                        magicDesc.SetActive(true);
                        magicDesc.GetComponent<RectTransform>().GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text = playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualSkill().nameSkill;
                        magicDesc.GetComponent<RectTransform>().GetChild(1).GetComponent<TMPro.TextMeshProUGUI>().text = "MP Cost: "+playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualSkill().mpcost;
                        magicDesc.GetComponent<RectTransform>().GetChild(2).GetComponent<TMPro.TextMeshProUGUI>().text = playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualSkill().description;
                    }
                    else
                        magicDesc.SetActive(false);

                    if (playerEventSystem.GetComponent<EventSystem>().currentSelectedGameObject == limit)
                    {
                        limitDesc.SetActive(true);
                        limitDesc.GetComponent<RectTransform>().GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text = playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualLimit().nameSkill;
                        limitDesc.GetComponent<RectTransform>().GetChild(1).GetComponent<TMPro.TextMeshProUGUI>().text = "MP Cost: " + playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualLimit().mpcost;
                        limitDesc.GetComponent<RectTransform>().GetChild(2).GetComponent<TMPro.TextMeshProUGUI>().text = playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualLimit().description;
                    }
                    else
                        limitDesc.SetActive(false);
                }
                break;
            case phaseType.PLAYER_ATTACK_CHOOSE_ENEMY:
                if (Input.GetKeyDown(KeyCode.DownArrow))
                    chooseEnemy++;
                else if (Input.GetKeyDown(KeyCode.UpArrow))
                    chooseEnemy--;

                if (chooseEnemy == enemiesInFight.Count)
                    chooseEnemy = 0;
                else if (chooseEnemy == -1)
                    chooseEnemy = enemiesInFight.Count - 1;

                pointerEnemy.SetActive(true);
                pointerEnemy.transform.position = enemiesInFight[chooseEnemy].transform.position + new Vector3(1f, 0f, 0);

                returnPhaseAttack();

                if (Input.GetKeyDown(KeyCode.A))
                    PlayerPhaseAttack();

                break;

            case phaseType.PLAYER_SKILL_CHOOSE_ENEMY:

                if (Input.GetKeyDown(KeyCode.DownArrow))
                    chooseEnemy++;
                else if (Input.GetKeyDown(KeyCode.UpArrow))
                    chooseEnemy--;

                if (chooseEnemy == enemiesInFight.Count)
                    chooseEnemy = 0;
                else if (chooseEnemy == -1)
                    chooseEnemy = enemiesInFight.Count - 1;

                pointerEnemy.SetActive(true);
                pointerEnemy.transform.position = enemiesInFight[chooseEnemy].transform.position + new Vector3(1f, 0f, 0);

                returnPhaseAttack();

                if (Input.GetKeyDown(KeyCode.A))
                {
                    if(playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualSkill().mpcost <= playersInFight[actualTorn].GetComponent<scr_Character>().stats.mp)
                    {
                        playersInFight[actualTorn].GetComponent<scr_Character>().stats.mp -= playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualSkill().mpcost;
                        PlayerSkillToEnemy();
                    }  
                }
                break;

            case phaseType.PLAYER_SKILL_CHOOSE_PLAYER:
                if (Input.GetKeyDown(KeyCode.DownArrow))
                    choosePlayer++;
                else if (Input.GetKeyDown(KeyCode.UpArrow))
                    choosePlayer--;

                if (choosePlayer == playersInFight.Count)
                    choosePlayer = 0;
                else if (choosePlayer == -1)
                    choosePlayer = playersInFight.Count - 1;

                pointerEnemy.SetActive(true);
                pointerEnemy.transform.position = playersInFight[choosePlayer].transform.position + new Vector3(1f, 0f, 0);

                returnPhaseAttack();

                if (Input.GetKeyDown(KeyCode.A))
                {
                    if (playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualSkill().mpcost <= playersInFight[actualTorn].GetComponent<scr_Character>().stats.mp)
                    {
                        playersInFight[actualTorn].GetComponent<scr_Character>().stats.mp -= playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualSkill().mpcost;
                        PlayerSkillToCharacter();
                    }
                }
                break;

            case phaseType.PLAYER_LIMIT_CHOOSE_ENEMY:

                if (Input.GetKeyDown(KeyCode.DownArrow))
                    chooseEnemy++;
                else if (Input.GetKeyDown(KeyCode.UpArrow))
                    chooseEnemy--;

                if (chooseEnemy == enemiesInFight.Count)
                    chooseEnemy = 0;
                else if (chooseEnemy == -1)
                    chooseEnemy = enemiesInFight.Count - 1;

                pointerEnemy.SetActive(true);
                pointerEnemy.transform.position = enemiesInFight[chooseEnemy].transform.position + new Vector3(1f, 0f, 0);

                returnPhaseAttack();

                if (Input.GetKeyDown(KeyCode.A))
                {
                    if (playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualLimit().mpcost <= playersInFight[actualTorn].GetComponent<scr_Character>().stats.mp)
                    {
                        playersInFight[actualTorn].GetComponent<scr_Character>().stats.mp -= playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualLimit().mpcost;
                        PlayerLimitToEnemy();
                    }
                }
                break;

            case phaseType.PLAYER_LIMIT_CHOOSE_PLAYER:
                if (Input.GetKeyDown(KeyCode.DownArrow))
                    choosePlayer++;
                else if (Input.GetKeyDown(KeyCode.UpArrow))
                    choosePlayer--;

                if (choosePlayer == playersInFight.Count)
                    choosePlayer = 0;
                else if (choosePlayer == -1)
                    choosePlayer = playersInFight.Count - 1;

                pointerEnemy.SetActive(true);
                pointerEnemy.transform.position = playersInFight[choosePlayer].transform.position + new Vector3(1f, 0f, 0);

                returnPhaseAttack();

                if (Input.GetKeyDown(KeyCode.A))
                {
                    if (playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualLimit().mpcost <= playersInFight[actualTorn].GetComponent<scr_Character>().stats.mp)
                    {
                        playersInFight[actualTorn].GetComponent<scr_Character>().stats.mp -= playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualLimit().mpcost;
                        PlayerLimitToCharacter();
                    }
                }
                break;

            case phaseType.ENEMY_TURN:
                EnemyPhaseAttack();
                canvasPlayerPhase.SetActive(false);
                break;
            case phaseType.INVENTORY:
                canvasPlayerPhase.SetActive(false);
                break;
            case phaseType.WIN_TURN:
                canvasPlayerPhase.SetActive(false);
                break;
        }
        
       
    }

    IEnumerator delayPlaySFX()
    {
        yield return new WaitForSeconds(0.5f);
        yield return new WaitForSeconds(0.5f);
        musicmanager.PlaySFXStandBy(playersInFight[actualTorn].GetComponent<scr_Character>().stats.nameCharacter);
    }

    private void Debug()
    {
       // if (playersInFight[actualTorn] != null)
      //  {
           // debugFight.GetComponent<TMPro.TextMeshProUGUI>().text = actualPhase + " - TURNO: " + playersInFight[actualTorn].gameObject.name;
            for (int i = 0; i < playersInFight.Count; i++)
            {
                debugFight.GetComponent<TMPro.TextMeshProUGUI>().text =  playersInFight[i].GetComponent<scr_Character>().stats.nameCharacter + " LVL:" + playersInFight[i].GetComponent<scr_Character>().stats.lvl + " HP: " + playersInFight[i].GetComponent<scr_Character>().stats.hp + "/" + playersInFight[i].GetComponent<scr_Character>().stats.maxhp;
                debugFight.GetComponent<TMPro.TextMeshProUGUI>().text += " actualATK: " + playersInFight[i].GetComponent<scr_Character>().getActualAtk();
                debugFight.GetComponent<TMPro.TextMeshProUGUI>().text += " actualDef: " + playersInFight[i].GetComponent<scr_Character>().getActualDef();
            }
            for (int i = 0; i < enemiesInFight.Count; i++)
            {
                debugFight.GetComponent<TMPro.TextMeshProUGUI>().text += "\n" + enemiesInFight[i].GetComponent<scr_Enemy>().stats.name + " LVL:" + enemiesInFight[i].GetComponent<scr_Enemy>().getActualLvl() + " HP: " + enemiesInFight[i].GetComponent<scr_Enemy>().getActualHP() + "/" + enemiesInFight[i].GetComponent<scr_Enemy>().getActualHPMax();
                debugFight.GetComponent<TMPro.TextMeshProUGUI>().text += " actualATK: " + enemiesInFight[i].GetComponent<scr_Enemy>().getActualAtk();
                debugFight.GetComponent<TMPro.TextMeshProUGUI>().text += " actualDef: " + enemiesInFight[i].GetComponent<scr_Enemy>().getActualDef();
            }
       // }
    }

    public void returnFight()
    {
        foreach (GameObject go in playersInFight)
        {
            go.GetComponent<scr_Character>().updateHPMPEvent();
        }
        actualPhase = phaseType.PLAYER_TURN;
        inventory.SetActive(false);
    }

    public void returnFightWastedTurn()
    {
        foreach (GameObject go in playersInFight)
        {
            go.GetComponent<scr_Character>().updateHPMPEvent();
        }
        actualPhase = phaseType.PLAYER_TURN;
        canvasStatePlayer[actualTorn].GetComponent<Image>().color = new Color(0.65f, 0.65f, 0.65f, 0.8f);
        playersInFight[actualTorn].GetComponent<Animator>().SetInteger("StateMachine", 0);
        actualTorn++;
        canPlay = true;
        if (actualTorn == playersInFight.Count)
        {
            actualTorn = 0;
            actualPhase = phaseType.ENEMY_TURN;
            uiPhase.text = "Enemy Phase";
            StartCoroutine(delayPhaseEnemy());
        }
        inventory.SetActive(false);
    }

    public void openInventory()
    {
        actualPhase = phaseType.INVENTORY;
        inventory.SetActive(true);
    }

    public List<GameObject> getPlayersInFight()
    {
        return playersInFight;
    }

    public void returnPhaseAttack()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            actualPhase = phaseType.PLAYER_TURN;
            pointerEnemy.SetActive(false);
        }
    }

    public void PlayerPhaseAttack()
    {
        playersInFight[actualTorn].GetComponent<Animator>().SetInteger("StateMachine", 2);
        musicmanager.PlaySFXAttack(playersInFight[actualTorn].GetComponent<scr_Character>().stats.nameCharacter);
        enemiesInFight[chooseEnemy].GetComponent<scr_Enemy>().Hurt(playersInFight[actualTorn].GetComponent<scr_Character>().Attack());
        CheckEnemyDeads();
        actualPhase = phaseType.PLAYER_TURN;
        //ACABA EL TURNO DEL JUGADOR
        canvasStatePlayer[actualTorn].GetComponent<Image>().color = new Color(0.65f, 0.65f, 0.65f, 0.8f);
        actualTorn++;
        canPlay = true;

        if (actualTorn == playersInFight.Count)
        {
            actualTorn = 0;
            actualPhase = phaseType.ENEMY_TURN;
            uiPhase.text = "Enemy Phase";
            StartCoroutine(delayPhaseEnemy());
        }

        pointerEnemy.SetActive(false);
        CheckEnemyDeads();
    }

    public void PlayerSkillToEnemy()
    {
        playersInFight[actualTorn].GetComponent<Animator>().SetInteger("StateMachine", 3);
        musicmanager.PlaySFXSkill(playersInFight[actualTorn].GetComponent<scr_Character>().stats.nameCharacter);

        switch (playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualSkill().type)
        {
            case so_Skills.TypeSkill.ATACK:
                enemiesInFight[chooseEnemy].GetComponent<scr_Enemy>().Hurt(playersInFight[actualTorn].GetComponent<scr_Character>().Skill());
                break;
            case so_Skills.TypeSkill.DEBUFF_ATK:
                enemiesInFight[chooseEnemy].GetComponent<scr_Enemy>().debuffAtk(playersInFight[actualTorn].GetComponent<scr_Character>().Skill());
                break;
            case so_Skills.TypeSkill.DEBUFF_DEF:
                enemiesInFight[chooseEnemy].GetComponent<scr_Enemy>().debuffDef(playersInFight[actualTorn].GetComponent<scr_Character>().Skill());
                break;
        }

        actualPhase = phaseType.PLAYER_TURN;
        //ACABA EL TURNO DEL JUGADOR
        canvasStatePlayer[actualTorn].GetComponent<Image>().color = new Color(0.65f, 0.65f, 0.65f, 0.8f);
        actualTorn++;
        canPlay = true;

        if (actualTorn == playersInFight.Count)
        {
            actualTorn = 0;
            actualPhase = phaseType.ENEMY_TURN;
            uiPhase.text = "Enemy Phase";
            StartCoroutine(delayPhaseEnemy());
        }

        pointerEnemy.SetActive(false);
        CheckEnemyDeads();
    }

    public void PlayerSkillToCharacter()
    {
        musicmanager.PlaySFXSkill(playersInFight[actualTorn].GetComponent<scr_Character>().stats.nameCharacter);
        playersInFight[actualTorn].GetComponent<Animator>().SetInteger("StateMachine", 3);

        switch (playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualSkill().type)
        {
            case so_Skills.TypeSkill.HEAL:
                playersInFight[choosePlayer].GetComponent<scr_Character>().Heal(playersInFight[actualTorn].GetComponent<scr_Character>().Skill());
                break;
            case so_Skills.TypeSkill.BUFF_ATK:
                playersInFight[choosePlayer].GetComponent<scr_Character>().BuffAttack(playersInFight[actualTorn].GetComponent<scr_Character>().Skill());
                break;
            case so_Skills.TypeSkill.BUFF_DEF:
                playersInFight[choosePlayer].GetComponent<scr_Character>().BuffDefense(playersInFight[actualTorn].GetComponent<scr_Character>().Skill());
                break;
        }

        actualPhase = phaseType.PLAYER_TURN;
        //ACABA EL TURNO DEL JUGADOR
        canvasStatePlayer[actualTorn].GetComponent<Image>().color = new Color(0.65f, 0.65f, 0.65f, 0.8f);
        actualTorn++;
        canPlay = true;

        if (actualTorn == playersInFight.Count)
        {
            actualTorn = 0;
            actualPhase = phaseType.ENEMY_TURN;
            uiPhase.text = "Enemy Phase";
            StartCoroutine(delayPhaseEnemy());
        }

        pointerEnemy.SetActive(false);
        CheckEnemyDeads();
    }

    public void PlayerLimitToEnemy()
    {
        musicmanager.PlaySFXLimit(playersInFight[actualTorn].GetComponent<scr_Character>().stats.nameCharacter);
        playersInFight[actualTorn].GetComponent<Animator>().SetInteger("StateMachine", 4);

        switch (playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualLimit().type)
        {
            case so_Skills.TypeSkill.ATACK:
                enemiesInFight[chooseEnemy].GetComponent<scr_Enemy>().Hurt(playersInFight[actualTorn].GetComponent<scr_Character>().Limit());
                break;
            case so_Skills.TypeSkill.DEBUFF_ATK:
                enemiesInFight[chooseEnemy].GetComponent<scr_Enemy>().debuffAtk(playersInFight[actualTorn].GetComponent<scr_Character>().Limit());
                break;
            case so_Skills.TypeSkill.DEBUFF_DEF:
                enemiesInFight[chooseEnemy].GetComponent<scr_Enemy>().debuffDef(playersInFight[actualTorn].GetComponent<scr_Character>().Limit());
                break;
        }

        actualPhase = phaseType.PLAYER_TURN;
        //ACABA EL TURNO DEL JUGADOR
        canvasStatePlayer[actualTorn].GetComponent<Image>().color = new Color(0.65f, 0.65f, 0.65f, 0.8f);
        actualTorn++;
        canPlay = true;

        if (actualTorn == playersInFight.Count)
        {
            actualTorn = 0;
            actualPhase = phaseType.ENEMY_TURN;
            uiPhase.text = "Enemy Phase";
            StartCoroutine(delayPhaseEnemy());
        }

        pointerEnemy.SetActive(false);
        CheckEnemyDeads();
    }

    public void PlayerLimitToCharacter()
    {
        musicmanager.PlaySFXLimit(playersInFight[actualTorn].GetComponent<scr_Character>().stats.nameCharacter);
        playersInFight[actualTorn].GetComponent<Animator>().SetInteger("StateMachine", 4);

        switch (playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualLimit().type)
        {
            case so_Skills.TypeSkill.HEAL:
                playersInFight[choosePlayer].GetComponent<scr_Character>().Heal(playersInFight[actualTorn].GetComponent<scr_Character>().Limit());
                break;
            case so_Skills.TypeSkill.BUFF_ATK:
                playersInFight[choosePlayer].GetComponent<scr_Character>().BuffAttack(playersInFight[actualTorn].GetComponent<scr_Character>().Limit());
                break;
            case so_Skills.TypeSkill.BUFF_DEF:
                playersInFight[choosePlayer].GetComponent<scr_Character>().BuffDefense(playersInFight[actualTorn].GetComponent<scr_Character>().Limit());
                break;
        }

        actualPhase = phaseType.PLAYER_TURN;
        //ACABA EL TURNO DEL JUGADOR
        canvasStatePlayer[actualTorn].GetComponent<Image>().color = new Color(0.65f, 0.65f, 0.65f, 0.8f);
        actualTorn++;
        canPlay = true;

        if (actualTorn == playersInFight.Count)
        {
            actualTorn = 0;
            actualPhase = phaseType.ENEMY_TURN;
            uiPhase.text = "Enemy Phase";
            StartCoroutine(delayPhaseEnemy());
        }

        pointerEnemy.SetActive(false);
        CheckEnemyDeads();
    }

    public void PlayerSkillAttack()
    {
        if (playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualSkill().mpcost <= playersInFight[actualTorn].GetComponent<scr_Character>().stats.mp)
        {
            canvasPlayerPhase.SetActive(false);
            switch (playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualSkill().numTargets)
            {
                case so_Skills.Targets.ONE_TARGET:
                    if (playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualSkill().type == so_Skills.TypeSkill.ATACK ||
                        playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualSkill().type == so_Skills.TypeSkill.DEBUFF_ATK ||
                        playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualSkill().type == so_Skills.TypeSkill.DEBUFF_DEF)
                    {
                        actualPhase = phaseType.PLAYER_SKILL_CHOOSE_ENEMY;
                    }
                    else
                    {
                        actualPhase = phaseType.PLAYER_SKILL_CHOOSE_PLAYER;
                    }
                    break;
                case so_Skills.Targets.ALL_TARGETS:
                    playersInFight[actualTorn].GetComponent<scr_Character>().stats.mp -= playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualSkill().mpcost;
                    musicmanager.PlaySFXSkill(playersInFight[actualTorn].GetComponent<scr_Character>().stats.nameCharacter);
                    playersInFight[actualTorn].GetComponent<Animator>().SetInteger("StateMachine", 3);

                    playersInFight[actualTorn].GetComponent<scr_Character>().Skill(playersInFight, enemiesInFight);
                    actualPhase = phaseType.PLAYER_TURN;
                    //ACABA EL TURNO DEL JUGADOR
                    canvasStatePlayer[actualTorn].GetComponent<Image>().color = new Color(0.65f, 0.65f, 0.65f, 0.8f);
                    actualTorn++;
                    canPlay = true;

                    if (actualTorn == playersInFight.Count)
                    {
                        actualTorn = 0;
                        actualPhase = phaseType.ENEMY_TURN;
                        uiPhase.text = "Enemy Phase";
                        StartCoroutine(delayPhaseEnemy());
                    }
                    pointerEnemy.SetActive(false);
                    CheckEnemyDeads();
                    break;
            }
        }
       
    }

    public void PlayerLimitAttack()
    {
        if (playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualLimit().mpcost <= playersInFight[actualTorn].GetComponent<scr_Character>().stats.mp)
        {

            canvasPlayerPhase.SetActive(false);
            switch (playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualLimit().numTargets)
            {
                case so_Skills.Targets.ONE_TARGET:
                    if (playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualLimit().type == so_Skills.TypeSkill.ATACK ||
                        playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualLimit().type == so_Skills.TypeSkill.DEBUFF_ATK ||
                        playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualLimit().type == so_Skills.TypeSkill.DEBUFF_DEF)
                    {
                        actualPhase = phaseType.PLAYER_LIMIT_CHOOSE_ENEMY;
                    }
                    else
                    {
                        actualPhase = phaseType.PLAYER_LIMIT_CHOOSE_PLAYER;
                    }
                    break;
                case so_Skills.Targets.ALL_TARGETS:
                    musicmanager.PlaySFXLimit(playersInFight[actualTorn].GetComponent<scr_Character>().stats.nameCharacter);
                    playersInFight[actualTorn].GetComponent<Animator>().SetInteger("StateMachine", 4);
                    playersInFight[actualTorn].GetComponent<scr_Character>().stats.mp -= playersInFight[actualTorn].GetComponent<scr_Character>().stats.actualLimit().mpcost;

                    playersInFight[actualTorn].GetComponent<scr_Character>().Limit(playersInFight, enemiesInFight);
                    actualPhase = phaseType.PLAYER_TURN;
                    //ACABA EL TURNO DEL JUGADOR
                    canvasStatePlayer[actualTorn].GetComponent<Image>().color = new Color(0.65f, 0.65f, 0.65f, 0.8f);
                    actualTorn++;
                    canPlay = true;

                    if (actualTorn == playersInFight.Count)
                    {
                        actualTorn = 0;
                        actualPhase = phaseType.ENEMY_TURN;
                        uiPhase.text = "Enemy Phase";
                        StartCoroutine(delayPhaseEnemy());
                    }
                    pointerEnemy.SetActive(false);
                    CheckEnemyDeads();
                    break;
            }
        }

    }

    public void EnemyPhaseAttack()
    {
        for(int i = 0; i < actualEnemies.Length; i++) //posa la ui en vermell quan és el torn del enemic
        {
            if(actualEnemies[i] == enemiesInFight[actualTorn])
            {
                canvasStateEnemy[i].GetComponent<Image>().color = new Color(1f, 0.33f, 0.33f, 0.8f);

            }
        }
        if (canAttackEnemy)
        {
            List<GameObject> alivePlayers = new List<GameObject>();
            foreach (GameObject g in playersInFight)
            {
                if (!g.GetComponent<scr_Character>().isDead())
                {
                    alivePlayers.Add(g);
                }
            }

            if(alivePlayers.Count == 0)
            {
                print("TODOS MUERTOS");
                SceneManager.LoadScene(persistence.scene);
            }
            else
            { 
                int random = UnityEngine.Random.Range(0, alivePlayers.Count);
                print(random);
                alivePlayers[random].GetComponent<scr_Character>().Hurt(enemiesInFight[actualTorn].GetComponent<scr_Enemy>().Attack());
                musicmanager.PlaySFXDamage(alivePlayers[random].GetComponent<scr_Character>().stats.nameCharacter);
                checkPlayerDead();
                canAttackEnemy = false;
                print("actualtorn:" + actualTorn);

                for (int i = 0; i < actualEnemies.Length; i++)//posa la ui en gris quan és el torn del enemic
                {
                    if (actualEnemies[i] == enemiesInFight[actualTorn])
                    {
                        canvasStateEnemy[i].GetComponent<Image>().color = new Color(0.65f, 0.65f, 0.65f, 0.8f);

                    }
                }
                StartCoroutine(delayPhaseEnemy());
            }

            actualTorn++;
            if (actualTorn == enemiesInFight.Count)
            {
                actualTorn = 0;
                uiPhase.text = "Player Phase";
                actualPhase = phaseType.PLAYER_TURN;
            }
            CheckEnemyDeads();
        }
    }

    public void CheckEnemyDeads()
    {
        //ESTO SE PONE A FINAL DE UN TURNO PERO LO PONGO AQUI PROVISIONALMENTE
        var lista = enemiesInFight;
        for (var i = enemiesInFight.Count - 1; i >= 0; i--)
        {
            if (enemiesInFight[i].GetComponent<scr_Enemy>().isDead())
            {
                enemiesInFight[i].SetActive(false);
                enemiesInFight.RemoveAt(i);
            }
        }

        //WIN
        if (enemiesInFight.Count == 0)
        {
            actualEnemies[0].GetComponent<scr_Enemy>().stats.active = false;
            actualPhase = phaseType.WIN_TURN;
            StartCoroutine(delayToMainScene());
        }
    }

    IEnumerator delayToMainScene()
    {
        results.SetActive(true);
        int totalXP = 0; //PONER AQUI EL DINERO QUE GANAN EN LA BATALLA
        int totalMoney = 0;
        results.GetComponent<RectTransform>().GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text = "RESULTS\n";
        //XP
        for (int j = 0; j < myActualTeam.Length; j++)
        {
            for (int i = 0; i < actualEnemies.Length; i++)
            {
                totalXP = actualEnemies[i].GetComponent<scr_Enemy>().getXP();
                myActualTeam[j].GetComponent<scr_Character>().stats.upExp(actualEnemies[i].GetComponent<scr_Enemy>().getXP());
            }
            results.GetComponent<RectTransform>().GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text += myActualTeam[j].GetComponent<scr_Character>().stats.name + " Lv:" + myActualTeam[j].GetComponent<scr_Character>().stats.lvl + " - Next Lv: " + (myActualTeam[j].GetComponent<scr_Character>().stats.xpNextLevel - myActualTeam[j].GetComponent<scr_Character>().stats.xp) + "\n";
        }
        //MONEY
        for (int i = 0; i < actualEnemies.Length; i++)
        {
            totalMoney += actualEnemies[i].GetComponent<scr_Enemy>().getActualLvl() * 250;
        }
        inv.money += totalMoney;

        results.GetComponent<RectTransform>().GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text += "Total XP per character: "+ totalXP;
        results.GetComponent<RectTransform>().GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text += "\nTotal money earned: " + totalMoney;

        List<GameObject> aliveChar = new List<GameObject>();
        foreach (GameObject g in playersInFight)
        {
            if (!g.GetComponent<scr_Character>().isDead())
            {
                g.GetComponent<Animator>().SetInteger("StateMachine", 5);
                aliveChar.Add(g);
            }
        }

        int r = UnityEngine.Random.Range(0, aliveChar.Count);
        musicmanager.PlaySFXWin(aliveChar[r].GetComponent<scr_Character>().stats.nameCharacter);

        //MOSTRAR RECOMPENSAS
        yield return new WaitForSeconds(5f);
        SceneManager.LoadScene(persistence.scene);
    }


    private void checkPlayerDead()
    {
        
        foreach(GameObject go in playersInFight)
        {
            if(go.GetComponent<scr_Character>().isDead() && go.GetComponent<Animator>().GetInteger("StateMachine") != 7) {
                musicmanager.PlaySFXDead(playersInFight[actualTorn].GetComponent<scr_Character>().stats.nameCharacter);
                go.GetComponent<Animator>().SetInteger("StateMachine", 6);
            }
        }
        /*
        if (playersInFight[actualTorn].GetComponent<scr_Character>().isDead())
        {
            playersInFight[actualTorn].GetComponent<Animator>().SetInteger("StateMachine", 6);
            actualTorn++;
            if (actualTorn == playersInFight.Count)
            {
                actualTorn = 0;
                actualPhase = phaseType.ENEMY_TURN;
                StartCoroutine(delayPhaseEnemy());
            }
        }*/
    }


    IEnumerator delayPhaseEnemy()
    {
        canAttackEnemy = false;
        yield return new WaitForSeconds(2.5f);
        canAttackEnemy = true;
    }

    public void PlayerPhaseChoosingEnemyToAttack()
    {
        canvasPlayerPhase.SetActive(false);
        actualPhase = phaseType.PLAYER_ATTACK_CHOOSE_ENEMY;
    }

    private void instanciarAliados()
    {
        myActualTeam = myActualTeamSO.myActualTeam;

        float auxTransformY = -1.5f;
        float auxTransformX = -0.8f;
        for (int i = 0; i < myActualTeam.Length; i++)
        {
            if (myActualTeam.Length == 1) {
                teamGeneralTransform.y += auxTransformY;
                teamGeneralTransform.x += auxTransformX;
                playersInFight.Add(Instantiate(myActualTeam[i], teamGeneralTransform, Quaternion.identity));
            }
            if (myActualTeam.Length == 2)
            {
                myActualTeam[i].transform.GetComponentInChildren<Renderer>().sortingOrder = i+1;
                playersInFight.Add(Instantiate(myActualTeam[i], teamGeneralTransform, Quaternion.identity));
                teamGeneralTransform.y += auxTransformY;
                teamGeneralTransform.x += auxTransformX;
            }
            if (myActualTeam.Length == 3)
            {
                myActualTeam[i].transform.GetComponentInChildren<Renderer>().sortingOrder = i + 1;
                playersInFight.Add(Instantiate(myActualTeam[i], teamGeneralTransform, Quaternion.identity));
                teamGeneralTransform.y += auxTransformY;
                teamGeneralTransform.x += auxTransformX;
            }
            if (myActualTeam.Length == 4)
            {
                myActualTeam[i].transform.GetComponentInChildren<Renderer>().sortingOrder = i+1;
                playersInFight.Add(Instantiate(myActualTeam[i], teamGeneralTransform, Quaternion.identity));
                teamGeneralTransform.y += auxTransformY;
                teamGeneralTransform.x += auxTransformX;
            }
            canvasStatePlayer[i].SetActive(true);
            canvasStatePlayer[i].GetComponent<Image>().color = new Color(0.65f, 0.65f, 0.65f, 0.8f);

        }
        int cont = 0;
        foreach(GameObject go in playersInFight)
        {
            go.GetComponent<scr_Character>().setHPMPEvent(hpEventPlayer[cont], mpEventPlayer[cont]);
            go.GetComponent<scr_Character>().updateHPMPEvent();
            if (go.GetComponent<scr_Character>().isDead())
            {
                go.GetComponent<Animator>().SetInteger("StateMachine", 7);
            }
            cont++;
        }
    }

    private void instanciarEnemigos()
    {
        int auxTransformY = -2;
        int auxTransformX = 1;
        for (int i = 0; i < enemiesArray.Length; i++) {
            if (persistence.enemigo.Equals(enemiesArray[i].name))
            {
                int qtEnemies = UnityEngine.Random.Range(1,4);
               // int qtEnemies = 3;
                actualEnemies = new GameObject[qtEnemies];
                for (int j = 0; j < qtEnemies; j++) {
                   // actualEnemies[j] = enemiesArray[i];
                    if(qtEnemies == 1)
                    {
                        enemyGeneralTransform.y += auxTransformY;
                        enemyGeneralTransform.x += auxTransformX;
                        actualEnemies[j] = Instantiate(enemiesArray[i], enemyGeneralTransform, Quaternion.identity);
                        actualEnemies[j].GetComponent<scr_Enemy>().setHPEvent(hpEventEnemy[j]);
                        enemiesInFight.Add(actualEnemies[j]);
                    }
                    else if(qtEnemies == 2)
                    {
                        actualEnemies[j] = Instantiate(enemiesArray[i], enemyGeneralTransform, Quaternion.identity);
                        actualEnemies[j].GetComponent<scr_Enemy>().setHPEvent(hpEventEnemy[j]);
                        enemiesInFight.Add(actualEnemies[j]);
                        enemyGeneralTransform.y += auxTransformY;
                        enemyGeneralTransform.x += auxTransformX;
                        
                    }
                    else if(qtEnemies == 3)
                    {
                        actualEnemies[j] = Instantiate(enemiesArray[i], enemyGeneralTransform, Quaternion.identity);
                        actualEnemies[j].GetComponent<scr_Enemy>().setHPEvent(hpEventEnemy[j]);
                        enemiesInFight.Add(actualEnemies[j]);
                        enemyGeneralTransform.y += auxTransformY;
                        enemyGeneralTransform.x += auxTransformX;
                    }
                    canvasStateEnemy[j].SetActive(true);
                }                
            }
           
        }
        float average = 0;
        foreach (GameObject chara in playersInFight)
        {
            average += chara.GetComponent<scr_Character>().stats.lvl;
        }
        average /= playersInFight.Count;
        
        foreach (GameObject enem in enemiesInFight)
        {
            int rand = UnityEngine.Random.Range((int)average - 1, (int)average + 2);
            
            if (rand <= 0)
                rand = 1;
            else if (rand > 10)
                rand = 10;

            enem.GetComponent<scr_Enemy>().setLvl(rand);
            enem.GetComponent<scr_Enemy>().updateHPEvent();
        }
    }

    public void Run()
    {
        actualEnemies[0].GetComponent<scr_Enemy>().stats.active = false;
        SceneManager.LoadScene(persistence.scene);
    }
}
