﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_BarMPEvent : ScriptableObject
{
    private readonly List<scr_BarMPEventListener> eventListeners = new List<scr_BarMPEventListener>();

    public void RaiseItem(GameObject it)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(it);
    }

    public void RegisterListener(scr_BarMPEventListener listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(scr_BarMPEventListener listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}