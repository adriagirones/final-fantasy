﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_BarMPEventListener : MonoBehaviour
{
    // Start is called before the first frame update
    [Tooltip("Event to register with.")]
    public so_BarMPEvent Event;
    public GameObject numMP;

    private float hpw;

    private void OnEnable()
    {
        hpw = this.GetComponent<RectTransform>().rect.width;
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(GameObject it)
    {

        if (it.transform.CompareTag("Character"))
        {
            this.GetComponent<RectTransform>().sizeDelta = new Vector2((it.GetComponent<scr_Character>().stats.mp * hpw) / it.GetComponent<scr_Character>().stats.getGlobalMaxMP(), this.GetComponent<RectTransform>().sizeDelta.y);
            numMP.GetComponent<TMPro.TextMeshProUGUI>().text = it.GetComponent<scr_Character>().stats.mp + "/" + it.GetComponent<scr_Character>().stats.getGlobalMaxMP();
        }
    }
}
