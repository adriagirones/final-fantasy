﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_BarHpEventListener : MonoBehaviour
{
    // Start is called before the first frame update
    [Tooltip("Event to register with.")]
    public so_BarHPEvent Event;
    public GameObject nameUI;
    public GameObject numHP;

    private float hpw;

    private void OnEnable()
    {
        hpw = this.GetComponent<RectTransform>().rect.width;
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(GameObject it)
    {
        
        if (it.transform.CompareTag("Enemy"))
        {
            this.GetComponent<RectTransform>().sizeDelta = new Vector2((it.GetComponent<scr_Enemy>().getActualHP() * hpw) / it.GetComponent<scr_Enemy>().getActualHPMax() , this.GetComponent<RectTransform>().sizeDelta.y);
            nameUI.GetComponent<TMPro.TextMeshProUGUI>().text = it.GetComponent<scr_Enemy>().stats.name + " Lv: " + it.GetComponent<scr_Enemy>().getActualLvl();
            numHP.GetComponent<TMPro.TextMeshProUGUI>().text = it.GetComponent<scr_Enemy>().getActualHP() + "/" + it.GetComponent<scr_Enemy>().getActualHPMax();
        }
        else if (it.transform.CompareTag("Character"))
        {
            this.GetComponent<RectTransform>().sizeDelta = new Vector2((it.GetComponent<scr_Character>().stats.hp * hpw) / it.GetComponent<scr_Character>().stats.getGlobalMaxHP(), this.GetComponent<RectTransform>().sizeDelta.y);
            nameUI.GetComponent<TMPro.TextMeshProUGUI>().text = it.GetComponent<scr_Character>().stats.name + " Lv: " + it.GetComponent<scr_Character>().stats.lvl;
            numHP.GetComponent<TMPro.TextMeshProUGUI>().text = it.GetComponent<scr_Character>().stats.hp + "/" + it.GetComponent<scr_Character>().stats.getGlobalMaxHP();
        }
    }
}
