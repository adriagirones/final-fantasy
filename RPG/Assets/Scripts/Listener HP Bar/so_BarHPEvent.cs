﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_BarHPEvent : ScriptableObject
{
    private readonly List<scr_BarHpEventListener> eventListeners =
          new List<scr_BarHpEventListener>();
    //HOLA

    public void RaiseItem(GameObject it)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(it);
    }

    public void RegisterListener(scr_BarHpEventListener listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(scr_BarHpEventListener listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}
