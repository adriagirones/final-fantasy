﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class scr_StartManager : MonoBehaviour// , ISelectHandler
{
    private bool changepos = false;
    public GameObject newgamebtn;
    public GameObject continuebtn;
    public EventSystem es;
    public Transform Puntero;

    //inventories
    public so_Inventory myInventory;
    public so_Inventory Shop1Inventory;
    public so_Inventory Shop2Inventory;

    //team
    public so_Team myTeam;
    public so_ActualTeam myActualTeam;

    //persistences
    public so_OverworldPJ Castle1Position;
    public so_OverworldPJ Castle2Position;
    public so_OverworldPJ CavePosition;
    public so_OverworldPJ ForestPosition;
    public so_OverworldPJ ForestBossPosition;
    public so_OverworldPJ InnPosition;
    public so_OverworldPJ OverWorldPJ;
    public so_OverworldPJ Village1Position;
    public so_OverworldPJ Village2Position;

    //pjs
    public so_Character A2;
    public so_Character Auron;
    public so_Character Domino;
    public so_Character Ishil;
    public so_Character Jade;
    public so_Character Malphasie;
    public so_Character Mercedes;
    public so_Character Roselia;
    public so_Character Tiz;
    public so_Character Victoria;

    public GameObject MercedesP;
    public GameObject RoseliaP;

    public so_UsableItem[] UsableItems;
    public so_UsableItem PotATK_10;
    public so_UsableItem PotDEF_10;
    public so_UsableItem PotMP_10;
    public so_UsableItem PotHP_10;

    public so_Enemy[] Enemies;
    public List<so_Weapon> Weapons = new List<so_Weapon>();
    public List<so_EquipmentAccessory> Accesories = new List<so_EquipmentAccessory>();
    

    public GameObject[] RecruitmentPrefab;


    //actual scene
    private string myScene = "";

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (es.currentSelectedGameObject == newgamebtn && !changepos)
        {
            print("newgame");
            Puntero.position = new Vector2(newgamebtn.GetComponent<RectTransform>().position.x - 2.5f, newgamebtn.GetComponent<RectTransform>().position.y);
            changepos = true;
        }
        else if (es.currentSelectedGameObject == continuebtn && changepos)
        {
            print("continue");
            Puntero.position = new Vector2(continuebtn.GetComponent<RectTransform>().position.x - 2.5f, continuebtn.GetComponent<RectTransform>().position.y);
            changepos = false;
        }
    }


    public void NewGame()
    {
        A2.hp = A2.hpLvl1;
        A2.mp = A2.mpLvl1;
        A2.maxhp = A2.hpLvl1;
        A2.maxmp = A2.mpLvl1;
        A2.atk = A2.atkLvl1;
        A2.def = A2.defLvl1;
        A2.lvl = 1;
        A2.xp = 0;
        A2.xpNextLevel = 100;
        A2.actualWeapon = null;
        A2.object1 = null;
        A2.object2 = null;

        Auron.hp = Auron.hpLvl1;
        Auron.mp = Auron.mpLvl1;
        Auron.maxhp = Auron.hpLvl1;
        Auron.maxmp = Auron.mpLvl1;
        Auron.atk = Auron.atkLvl1;
        Auron.def = Auron.defLvl1;
        Auron.lvl = 1;
        Auron.xp = 0;
        Auron.xpNextLevel = 100;
        Auron.actualWeapon = null;
        Auron.object1 = null;
        Auron.object2 = null;

        Domino.hp = Domino.hpLvl1;
        Domino.mp = Domino.mpLvl1;
        Domino.maxhp = Domino.hpLvl1;
        Domino.maxmp = Domino.mpLvl1;
        Domino.atk = Domino.atkLvl1;
        Domino.def = Domino.defLvl1;
        Domino.lvl = 1;
        Domino.xp = 0;
        Domino.xpNextLevel = 100;
        Domino.actualWeapon = null;
        Domino.object1 = null;
        Domino.object2 = null;

        Ishil.hp = Ishil.hpLvl1;
        Ishil.mp = Ishil.mpLvl1;
        Ishil.maxhp = Ishil.hpLvl1;
        Ishil.maxmp = Ishil.mpLvl1;
        Ishil.atk = Ishil.atkLvl1;
        Ishil.def = Ishil.defLvl1;
        Ishil.lvl = 1;
        Ishil.xp = 0;
        Ishil.xpNextLevel = 100;
        Ishil.actualWeapon = null;
        Ishil.object1 = null;
        Ishil.object2 = null;

        Jade.hp = Jade.hpLvl1;
        Jade.mp = Jade.mpLvl1;
        Jade.maxhp = Jade.hpLvl1;
        Jade.maxmp = Jade.mpLvl1;
        Jade.atk = Jade.atkLvl1;
        Jade.def = Jade.defLvl1;
        Jade.lvl = 1;
        Jade.xp = 0;
        Jade.xpNextLevel = 100;
        Jade.actualWeapon = null;
        Jade.object1 = null;
        Jade.object2 = null;

        Malphasie.hp = Malphasie.hpLvl1;
        Malphasie.mp = Malphasie.mpLvl1;
        Malphasie.maxhp = Malphasie.hpLvl1;
        Malphasie.maxmp = Malphasie.mpLvl1;
        Malphasie.atk = Malphasie.atkLvl1;
        Malphasie.def = Malphasie.defLvl1;
        Malphasie.lvl = 1;
        Malphasie.xp = 0;
        Malphasie.xpNextLevel = 100;
        Malphasie.actualWeapon = null;
        Malphasie.object1 = null;
        Malphasie.object2 = null;

        Mercedes.hp = Mercedes.hpLvl1;
        Mercedes.mp = Mercedes.mpLvl1;
        Mercedes.maxhp = Mercedes.hpLvl1;
        Mercedes.maxmp = Mercedes.mpLvl1;
        Mercedes.atk = Mercedes.atkLvl1;
        Mercedes.def = Mercedes.defLvl1;
        Mercedes.lvl = 1;
        Mercedes.xp = 0;
        Mercedes.xpNextLevel = 100;
        Mercedes.actualWeapon = null;
        Mercedes.object1 = null;
        Mercedes.object2 = null;

        Roselia.hp = Roselia.hpLvl1;
        Roselia.mp = Roselia.mpLvl1;
        Roselia.maxhp = Roselia.hpLvl1;
        Roselia.maxmp = Roselia.mpLvl1;
        Roselia.atk = Roselia.atkLvl1;
        Roselia.def = Roselia.defLvl1;
        Roselia.lvl = 1;
        Roselia.xp = 0;
        Roselia.xpNextLevel = 100;
        Roselia.actualWeapon = null;
        Roselia.object1 = null;
        Roselia.object2 = null;

        Tiz.hp = Tiz.hpLvl1;
        Tiz.mp = Tiz.mpLvl1;
        Tiz.maxhp = Tiz.hpLvl1;
        Tiz.maxmp = Tiz.mpLvl1;
        Tiz.atk = Tiz.atkLvl1;
        Tiz.def = Tiz.defLvl1;
        Tiz.lvl = 1;
        Tiz.xp = 0;
        Tiz.xpNextLevel = 100;
        Tiz.actualWeapon = null;
        Tiz.object1 = null;
        Tiz.object2 = null;

        Victoria.hp = Victoria.hpLvl1;
        Victoria.mp = Victoria.mpLvl1;
        Victoria.maxhp = Victoria.hpLvl1;
        Victoria.maxmp = Victoria.mpLvl1;
        Victoria.atk = Victoria.atkLvl1;
        Victoria.def = Victoria.defLvl1;
        Victoria.lvl = 1;
        Victoria.xp = 0;
        Victoria.xpNextLevel = 100;
        Victoria.actualWeapon = null;
        Victoria.object1 = null;
        Victoria.object2 = null;

        GameObject[] myAuxActTeam = new GameObject[2];
        myAuxActTeam.SetValue(MercedesP, 0);
        myAuxActTeam.SetValue(RoseliaP, 1);
        myActualTeam.myActualTeam = myAuxActTeam;

        List<GameObject> myAuxTeam = new List<GameObject>();
        myAuxTeam.Add(MercedesP);
        myAuxTeam.Add(RoseliaP);
        myTeam.Team = myAuxTeam;

        foreach(so_Enemy e in Enemies)
        {
            e.active = true;
        }

        Shop1Inventory.usableItems.Clear();
        Shop2Inventory.usableItems.Clear();
        foreach (so_UsableItem i in UsableItems)
        {
            i.quantity = 0;
            Shop1Inventory.usableItems.Add(i);
            Shop2Inventory.usableItems.Add(i);
        }

        myInventory.usableItems.Clear();
        PotATK_10.quantity = 1;
        PotDEF_10.quantity = 1;
        PotHP_10.quantity = 5;
        PotMP_10.quantity = 3;
        myInventory.usableItems.Add(PotATK_10);
        myInventory.usableItems.Add(PotDEF_10);
        myInventory.usableItems.Add(PotHP_10);
        myInventory.usableItems.Add(PotMP_10);
        myInventory.money = 5000;

        Shop1Inventory.weapons.Clear();
        Shop2Inventory.weapons.Clear();
        //SHUFFLE WEAPONS
        for (int i = 0; i < Weapons.Count; i++)
        {
            so_Weapon temp = Weapons[i];
            int randomIndex = Random.Range(i, Weapons.Count);
            Weapons[i] = Weapons[randomIndex];
            Weapons[randomIndex] = temp;
        }
        //ASSIGN WEAPONS SHOP
        for (int i = 0; i < Weapons.Count; i++)
        {
            Weapons[i].equiped = false;
            if (i < Weapons.Count / 2)
            {
                Shop1Inventory.weapons.Add(Weapons[i]);
            }
            else
            {
                Shop2Inventory.weapons.Add(Weapons[i]);
            }
        }

        Shop1Inventory.equipableObjects.Clear();
        Shop2Inventory.equipableObjects.Clear();
        //SHUFFLE ACCESSORIES
        for (int i = 0; i < Accesories.Count; i++)
        {
            so_EquipmentAccessory temp = Accesories[i];
            int randomIndex = Random.Range(i, Accesories.Count);
            Accesories[i] = Accesories[randomIndex];
            Accesories[randomIndex] = temp;
        }
        //ASSIGN
        for (int i = 0; i < Accesories.Count; i++)
        {
            Accesories[i].equiped = false;
            if (i < Accesories.Count / 2)
            {
                Shop1Inventory.equipableObjects.Add(Accesories[i]);
            }
            else
            {
                Shop2Inventory.equipableObjects.Add(Accesories[i]);
            }
        }

        Shop1Inventory.recruitmentChar.Clear();
        Shop2Inventory.recruitmentChar.Clear();

        foreach (GameObject r in RecruitmentPrefab)
        {
            Shop1Inventory.recruitmentChar.Add(r);
            Shop2Inventory.recruitmentChar.Add(r);
        }

        OverWorldPJ.PJtransform = new Vector3(2.85f, -2.02f, 0);
        Castle1Position.PJtransform = new Vector3(0.16f, -1.18f, 0);
        Castle2Position.PJtransform = new Vector3(0.34f, -0.84f, 0);
        CavePosition.PJtransform = new Vector3(8.442f, -16.16f, 0);
        ForestPosition.PJtransform = new Vector3(-7.04f, -3.57f, 0);
        ForestBossPosition.PJtransform = new Vector3(-7.04f, -3.811f, 0);
        InnPosition.PJtransform = new Vector3(-2.14f, -13.75f,  0);
        Village1Position.PJtransform = new Vector3(-6.04f, -24.09f,0);
        Village2Position.PJtransform = new Vector3(-0.09f, -2.57f,0);
        SceneManager.LoadScene("DaniScene");


    }

    public void cargarPartida()
    {
        //Inventarios
        if(File.Exists("Assets/json/saveGameMainInventory.json")) 
        {
            string jsonStrMainInventory = File.ReadAllText("Assets/json/saveGameMainInventory.json");
            JsonUtility.FromJsonOverwrite(jsonStrMainInventory, myInventory);
        }

        if (File.Exists("Assets/json/saveGameShop1Inventory.json"))
        {
            string jsonStrShop1Inventory = File.ReadAllText("Assets/json/saveGameShop1Inventory.json");
            JsonUtility.FromJsonOverwrite(jsonStrShop1Inventory, Shop1Inventory);
        }

        if (File.Exists("Assets/json/saveGameShop2Inventory.json"))
        {
            string jsonShop2Inventory = File.ReadAllText("Assets/json/saveGameShop2Inventory.json");
            JsonUtility.FromJsonOverwrite(jsonShop2Inventory, Shop2Inventory);
        }


        //Team

        if (File.Exists("Assets/json/jsonMyTeam.json"))
        {
            string jsonMyTeam = File.ReadAllText("Assets/json/jsonMyTeam.json");
            JsonUtility.FromJsonOverwrite(jsonMyTeam, myTeam);
        }

        if (File.Exists("Assets/json/jsonMyActualTeam.json"))
        {
            string jsonMyActualTeam = File.ReadAllText("Assets/json/jsonMyActualTeam.json");
            JsonUtility.FromJsonOverwrite(jsonMyActualTeam, myActualTeam);
        }

        string jsonMyActualScene = null;
        //escena
        if (File.Exists("Assets/json/jsonMyActualScene.json"))
        {
            jsonMyActualScene = File.ReadAllText("Assets/json/jsonMyActualScene.json");
        }
           
        
        if(jsonMyActualScene != null)
         JsonUtility.FromJsonOverwrite(jsonMyActualScene, myScene);
        if (myScene.Equals(""))
        {
            myScene = "DaniScene";
        }

        //persistence
        if (File.Exists("Assets/json/jsonCastle1Position.json"))
        {
            string jsonCastle1Position = File.ReadAllText("Assets/json/jsonCastle1Position.json");
            JsonUtility.FromJsonOverwrite(jsonCastle1Position, Castle1Position);
        }

        if (File.Exists("Assets/json/jsonCastle2Position.json"))
        {
            string jsonCastle2Position = File.ReadAllText("Assets/json/jsonCastle2Position.json");
            JsonUtility.FromJsonOverwrite(jsonCastle2Position, Castle2Position);
        }

        if (File.Exists("Assets/json/jsonCavePosition.json"))
        {
            string jsonCavePosition = File.ReadAllText("Assets/json/jsonCavePosition.json");
            JsonUtility.FromJsonOverwrite(jsonCavePosition, CavePosition);
        }


        if (File.Exists("Assets/json/jsonForestPosition.json"))
        {
            string jsonForestPosition = File.ReadAllText("Assets/json/jsonForestPosition.json");
            JsonUtility.FromJsonOverwrite(jsonForestPosition, ForestPosition);
        }

        if (File.Exists("Assets/json/jsonForestBossPosition.json"))
        {
            string jsonForestBossPosition = File.ReadAllText("Assets/json/jsonForestBossPosition.json");
            JsonUtility.FromJsonOverwrite(jsonForestBossPosition, ForestBossPosition);
        }

        if (File.Exists("Assets/json/jsonInnPosition.json"))
        {
            string jsonInnPosition = File.ReadAllText("Assets/json/jsonInnPosition.json");
            JsonUtility.FromJsonOverwrite(jsonInnPosition, InnPosition);
        }

        if (File.Exists("Assets/json/jsonOverWorldPJ.json"))
        {
            string jsonOverWorldPJ = File.ReadAllText("Assets/json/jsonOverWorldPJ.json");
            JsonUtility.FromJsonOverwrite(jsonOverWorldPJ, OverWorldPJ);
        }

        if (File.Exists("Assets/json/jsonVillage1Position.json"))
        {
            string jsonVillage1Position = File.ReadAllText("Assets/json/jsonVillage1Position.json");
            JsonUtility.FromJsonOverwrite(jsonVillage1Position, Village1Position);
        }

        if (File.Exists("Assets/json/jsonVillage2Position.json"))
        {
            string jsonVillage2Position = File.ReadAllText("Assets/json/jsonVillage2Position.json");
            JsonUtility.FromJsonOverwrite(jsonVillage2Position, Village2Position);
        }

        //pjs
        if (File.Exists("Assets/json/jsonA2.json"))
        {
            string jsonA2 = File.ReadAllText("Assets/json/jsonA2.json");
            JsonUtility.FromJsonOverwrite(jsonA2, A2);
        }

        if (File.Exists("Assets/json/jsonAuron.json"))
        {
            string jsonAuron = File.ReadAllText("Assets/json/jsonAuron.json");
            JsonUtility.FromJsonOverwrite(jsonAuron, Auron);
        }

        if (File.Exists("Assets/json/jsonDomino.json"))
        {
            string jsonDomino = File.ReadAllText("Assets/json/jsonDomino.json");
            JsonUtility.FromJsonOverwrite(jsonDomino, Domino);
        }

        if (File.Exists("Assets/json/jsonIshil.json"))
        {
            string jsonIshil = File.ReadAllText("Assets/json/jsonIshil.json");
            JsonUtility.FromJsonOverwrite(jsonIshil, Ishil);
        }

        if (File.Exists("Assets/json/jsonJade.json"))
        {
            string jsonJade = File.ReadAllText("Assets/json/jsonJade.json");
            JsonUtility.FromJsonOverwrite(jsonJade, Jade);
        }

        if (File.Exists("Assets/json/jsonMalphasie.json"))
        {
            string jsonMalphasie = File.ReadAllText("Assets/json/jsonMalphasie.json");
            JsonUtility.FromJsonOverwrite(jsonMalphasie, Malphasie);
        }

        if (File.Exists("Assets/json/jsonMercedes.json"))
        {
            string jsonMercedes = File.ReadAllText("Assets/json/jsonMercedes.json");
            JsonUtility.FromJsonOverwrite(jsonMercedes, Mercedes);
        }

        if (File.Exists("Assets/json/jsonRoselia.json"))
        {
            string jsonRoselia = File.ReadAllText("Assets/json/jsonRoselia.json");
            JsonUtility.FromJsonOverwrite(jsonRoselia, Roselia);
        }

        if (File.Exists("Assets/json/jsonTiz.json"))
        {
            string jsonTiz = File.ReadAllText("Assets/json/jsonTiz.json");
            JsonUtility.FromJsonOverwrite(jsonTiz, Tiz);
        }

        if (File.Exists("Assets/json/jsonVictoria.json"))
        {
            string jsonVictoria = File.ReadAllText("Assets/json/jsonVictoria.json");
            JsonUtility.FromJsonOverwrite(jsonVictoria, Victoria);
            SceneManager.LoadScene(myScene);
        }

      
    }

}
