﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System.IO;
using System;

public class scr_MenuManager : MonoBehaviour
{
    public so_Inventory inventory;

    private bool escPressed = false;
    private bool further = false;
    //private bool firstitem = false;
    public GameObject cvs;
    public GameObject Icvs;
    public GameObject Ecvs;
    public GameObject Statuscvs;
    public GameObject Formationcvs;
    public EventSystem es;
    public EventSystem Ies;

    public GameObject itemprefab;


    public GameObject PJ;

    //inventories
    public so_Inventory myInventory;
    public so_Inventory Shop1Inventory;
    public so_Inventory Shop2Inventory;

    //team
    public so_Team myTeam;
    public so_ActualTeam myActualTeam;

    //persistences
    public so_OverworldPJ Castle1Position;
    public so_OverworldPJ Castle2Position;
    public so_OverworldPJ CavePosition;
    public so_OverworldPJ ForestPosition;
    public so_OverworldPJ ForestBossPosition;
    public so_OverworldPJ InnPosition;
    public so_OverworldPJ OverWorldPJ;
    public so_OverworldPJ Village1Position;
    public so_OverworldPJ Village2Position;

    //pjs
    public so_Character A2;
    public so_Character Auron;
    public so_Character Domino;
    public so_Character Ishil;
    public so_Character Jade;
    public so_Character Malphasie;
    public so_Character Mercedes;
    public so_Character Roselia;
    public so_Character Tiz;
    public so_Character Victoria;

    //actual scene
    private String myScene;


    void Awake()
    {
        cvs.SetActive(false);
        Icvs.SetActive(false);
        Ecvs.SetActive(false);
        Statuscvs.SetActive(false);
        Formationcvs.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !escPressed)
        {
            if (PJ.gameObject != null)
                PJ.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;

            cvs.SetActive(true);
            escPressed = true;
            Ies.firstSelectedGameObject = cvs.transform.GetChild(2).gameObject;
            //es.firstSelectedGameObject=GameObject.Find("Return");
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && escPressed && !further)
        {
            if (PJ.gameObject != null)
                PJ.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;

            cvs.SetActive(false);
            escPressed = false;
        }
    }

    public void returnMenu()
    {
        cvs.SetActive(false);
        Icvs.SetActive(false);
        escPressed = false;
        further = false;
    }

    public void inventoryMenu()
    {
        //cvs.SetActive(false);
        Icvs.SetActive(true);
        further = true;
        //es.enabled = false;
        //es.gameObject.SetActive(false);
        //cvs.GetComponentInChildren<Button>().enabled = false;
        menuenabler(false);
        /*
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                GameObject item = Instantiate(itemprefab);
                item.transform.SetParent(Icvs.transform);
                item.GetComponent<RectTransform>().sizeDelta = new Vector2(100f, 20f);
                item.GetComponent<RectTransform>().localScale = new Vector3(0.8f, 0.8f, 0.8f);
                item.GetComponent<RectTransform>().position = new Vector2(j * 1.5f, -i);
                item.transform.GetChild(0).GetComponent<TextMeshProUGUI>().fontSize = 36;
                if (!firstitem)
                    Ies.firstSelectedGameObject = item;
                firstitem = true;

            }
        }*/
    }
    public void EquipmentMenu()
    {
        Ecvs.SetActive(true);
        further = true;
        menuenabler(false);
    }
    public void StatusMenu()
    {
        Statuscvs.SetActive(true);
        further = true;
        menuenabler(false);
    }
    public void FormationMenu()
    {
        Formationcvs.SetActive(true);
        further = true;
        menuenabler(false);
    }

    public void exitMenu()
    {
        SceneManager.LoadScene("Start");
    }

    public void returnMainMenu()
    {
        if (PJ.gameObject != null)
            PJ.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
        further = false;
        es.gameObject.SetActive(true);
        //cvs.GetComponentInChildren<Button>().enabled = true;
        menuenabler(true);
    }

    public void menuenabler(bool fag)
    {
        foreach (Button a in cvs.transform.GetComponentsInChildren<Button>())
        {
            a.enabled = fag;
        }
    }

    public void guardarPartida()
    {
        //Inventarios
        string jsonStrMainInventory = JsonUtility.ToJson(myInventory, true);
        File.WriteAllText("Assets/json/saveGameMainInventory.json", jsonStrMainInventory);

        string jsonStrShop1Inventory = JsonUtility.ToJson(Shop1Inventory, true);
        File.WriteAllText("Assets/json/saveGameShop1Inventory.json", jsonStrShop1Inventory);

        string jsonShop2Inventory = JsonUtility.ToJson(Shop2Inventory, true);
        File.WriteAllText("Assets/json/saveGameShop2Inventory.json", jsonShop2Inventory);

        //team

        string jsonMyTeam = JsonUtility.ToJson(myTeam, true);
        File.WriteAllText("Assets/json/jsonMyTeam.json", jsonMyTeam);

        string jsonMyActualTeam = JsonUtility.ToJson(myActualTeam, true);
        File.WriteAllText("Assets/json/jsonMyActualTeam.json", jsonMyActualTeam);

        //persistence

        string jsonCastle1Position = JsonUtility.ToJson(Castle1Position, true);
        File.WriteAllText("Assets/json/jsonCastle1Position.json", jsonCastle1Position);

        string jsonCastle2Position = JsonUtility.ToJson(Castle2Position, true);
        File.WriteAllText("Assets/json/jsonCastle2Position.json", jsonCastle2Position);

        string jsonCavePosition = JsonUtility.ToJson(CavePosition, true);
        File.WriteAllText("Assets/json/jsonCavePosition.json", jsonCavePosition);

        string jsonForestPosition = JsonUtility.ToJson(ForestPosition, true);
        File.WriteAllText("Assets/json/jsonForestPosition.json", jsonForestPosition);

        string jsonForestBossPosition = JsonUtility.ToJson(ForestBossPosition, true);
        File.WriteAllText("Assets/json/jsonForestBossPosition.json", jsonForestBossPosition);

        string jsonInnPosition = JsonUtility.ToJson(InnPosition, true);
        File.WriteAllText("Assets/json/jsonInnPosition.json", jsonInnPosition);

        string jsonOverWorldPJ = JsonUtility.ToJson(OverWorldPJ, true);
        File.WriteAllText("Assets/json/jsonOverWorldPJ.json", jsonOverWorldPJ);

        string jsonVillage1Position = JsonUtility.ToJson(Village1Position, true);
        File.WriteAllText("Assets/json/jsonVillage1Position.json", jsonVillage1Position);

        string jsonVillage2Position = JsonUtility.ToJson(Village2Position, true);
        File.WriteAllText("Assets/json/jsonVillage2Position.json", jsonVillage2Position);

        //pjs

        string jsonA2 = JsonUtility.ToJson(A2, true);
        File.WriteAllText("Assets/json/jsonA2.json", jsonA2);

        string jsonAuron = JsonUtility.ToJson(Auron, true);
        File.WriteAllText("Assets/json/jsonAuron.json", jsonAuron);

        string jsonDomino = JsonUtility.ToJson(Domino, true);
        File.WriteAllText("Assets/json/jsonDomino.json", jsonDomino);

        string jsonIshil = JsonUtility.ToJson(Ishil, true);
        File.WriteAllText("Assets/json/jsonIshil.json", jsonIshil);

        string jsonJade = JsonUtility.ToJson(Jade, true);
        File.WriteAllText("Assets/json/jsonJade.json", jsonJade);

        string jsonMalphasie = JsonUtility.ToJson(Malphasie, true);
        File.WriteAllText("Assets/json/jsonMalphasie.json", jsonMalphasie);

        string jsonMercedes = JsonUtility.ToJson(Mercedes, true);
        File.WriteAllText("Assets/json/jsonMercedes.json", jsonMercedes);

        string jsonRoselia = JsonUtility.ToJson(Roselia, true);
        File.WriteAllText("Assets/json/jsonRoselia.json", jsonRoselia);

        string jsonTiz = JsonUtility.ToJson(Tiz, true);
        File.WriteAllText("Assets/json/jsonTiz.json", jsonTiz);

        string jsonVictoria = JsonUtility.ToJson(Victoria, true);
        File.WriteAllText("Assets/json/jsonVictoria.json", jsonVictoria);


        //escena
        myScene = SceneManager.GetActiveScene().name;
        String jsonMyActualScene = JsonUtility.ToJson(myScene, true);
        File.WriteAllText("Assets/json/jsonMyActualScene.json", jsonMyActualScene);

    }

}
