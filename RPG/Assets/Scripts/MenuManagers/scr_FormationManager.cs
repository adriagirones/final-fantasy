﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class scr_FormationManager : MonoBehaviour
{
    public so_ActualTeam ActualTeam;
    public so_Team Team;

    private GameObject[] auxActualTeam;

    private List<GameObject> auxTeam = new List<GameObject>();

    public GameObject CharBar;
    private bool charchanged = false;
    int indexchar = 0;
    private enum menuphase
    {
        ChoosingChar,
        ChoosingNewChar,
        ChangingEquip
    }
    menuphase menu;

    //public so_GameEvent healingItemEvent;

    public scr_MenuManager menuManager;
    public GameObject Fcvs;


    public GameObject[] ChangeableCharList = new GameObject[8];
    private int index = 0;
    private bool indexchange = true;
    public GameObject SelectableChars;



    // Start is called before the first frame update
    void Start()
    {
        SelectableChars.SetActive(false);
        CharBar.SetActive(false);
        menu = menuphase.ChoosingChar;
        //CharChoosed.SetActive(false);

    }
    private void updateCharBar()
    {
        int indexcharbar = 0;
        //CharBar.transform.GetChild(0).gameObject.SetActive(false);
        //CharBar.transform.GetChild(1).gameObject.SetActive(false);
        //CharBar.transform.GetChild(2).gameObject.SetActive(false);
        //CharBar.transform.GetChild(3).gameObject.SetActive(false);

        foreach (GameObject go in ActualTeam.myActualTeam)
        {
            print("hola");
            //CharBar.transform.GetChild(indexcharbar).gameObject.SetActive(true);
            CharBar.transform.GetChild(indexcharbar).GetChild(0).GetComponent<Image>().color = Color.white;
            CharBar.transform.GetChild(indexcharbar).GetChild(0).GetComponent<Image>().sprite = ActualTeam.myActualTeam[indexcharbar].GetComponent<scr_Character>().stats.splashart;
            CharBar.transform.GetChild(indexcharbar).GetChild(1).GetComponent<TextMeshProUGUI>().text = ActualTeam.myActualTeam[indexcharbar].GetComponent<scr_Character>().stats.nameCharacter;
            CharBar.transform.GetChild(indexcharbar).GetChild(2).GetComponent<TextMeshProUGUI>().text = "LV: " + ActualTeam.myActualTeam[indexcharbar].GetComponent<scr_Character>().stats.lvl;
            CharBar.transform.GetChild(indexcharbar).GetChild(3).GetComponent<TextMeshProUGUI>().text = "Type: " + ActualTeam.myActualTeam[indexcharbar].GetComponent<scr_Character>().stats.type;
            CharBar.transform.GetChild(indexcharbar).GetChild(4).GetComponent<TextMeshProUGUI>().text = "HP : " + ActualTeam.myActualTeam[indexcharbar].GetComponent<scr_Character>().stats.hp + "/" + ActualTeam.myActualTeam[indexcharbar].GetComponent<scr_Character>().stats.getGlobalMaxHP();
            CharBar.transform.GetChild(indexcharbar).GetChild(5).GetComponent<TextMeshProUGUI>().text = "MP : " + ActualTeam.myActualTeam[indexcharbar].GetComponent<scr_Character>().stats.mp + "/" + ActualTeam.myActualTeam[indexcharbar].GetComponent<scr_Character>().stats.getGlobalMaxMP();
            indexcharbar++;
        }
    }

    private void updateSelectableChars()
    {
        for (int i = 0; i < ChangeableCharList.Length; i++)
        {
            ChangeableCharList[i].SetActive(false);
        }
        auxTeam = new List<GameObject>();
        bool dupe = false;

        foreach (GameObject go in Team.Team)
        {
            dupe = false;
            foreach (GameObject go2 in ActualTeam.myActualTeam)
            {
                if (go == go2)
                    dupe = true;
            }
            if (!dupe)
                auxTeam.Add(go);
        }

        for (int i = 0; i < auxTeam.Count; i++)
        {
            ChangeableCharList[i].SetActive(true);
            ChangeableCharList[i].transform.GetChild(0).GetComponent<Image>().sprite = auxTeam[i].GetComponent<scr_Character>().stats.splashart;
            ChangeableCharList[i].transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = auxTeam[i].GetComponent<scr_Character>().stats.nameCharacter;
            ChangeableCharList[i].transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "LV: " + auxTeam[i].GetComponent<scr_Character>().stats.lvl;
            ChangeableCharList[i].transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = "Type: " + auxTeam[i].GetComponent<scr_Character>().stats.type;
            ChangeableCharList[i].transform.GetChild(4).GetComponent<TextMeshProUGUI>().text = "HP:" + auxTeam[i].GetComponent<scr_Character>().stats.hp + "/" + auxTeam[i].GetComponent<scr_Character>().stats.getGlobalMaxHP() + " MP : " + auxTeam[i].GetComponent<scr_Character>().stats.mp + "/" + auxTeam[i].GetComponent<scr_Character>().stats.getGlobalMaxMP(); ;
        }

    }
    bool chang = true;
    private void choosecharacter()
    {
        if (chang)
        {
            indexchar = 0;
            CharBar.transform.GetChild(indexchar).GetComponent<Image>().color = new Color(1f, 0.33f, 0.33f, 0.8f);
            chang = false;
        }
        print(indexchar);

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (indexchar == 4 - 1)
            {
                CharBar.transform.GetChild(indexchar).GetComponent<Image>().color = new Color(0.65f, 0.65f, 0.65f, 0.8f);
                indexchar = 0;
                CharBar.transform.GetChild(indexchar).GetComponent<Image>().color = new Color(1f, 0.33f, 0.33f, 0.8f);
            }
            else
            {
                CharBar.transform.GetChild(indexchar).GetComponent<Image>().color = new Color(0.65f, 0.65f, 0.65f, 0.8f);
                indexchar++;
                CharBar.transform.GetChild(indexchar).GetComponent<Image>().color = new Color(1f, 0.33f, 0.33f, 0.8f);
            }
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (indexchar == 0)
            {
                CharBar.transform.GetChild(indexchar).GetComponent<Image>().color = new Color(0.65f, 0.65f, 0.65f, 0.8f);
                indexchar = 4 - 1;
                CharBar.transform.GetChild(indexchar).GetComponent<Image>().color = new Color(1f, 0.33f, 0.33f, 0.8f);
            }
            else
            {
                CharBar.transform.GetChild(indexchar).GetComponent<Image>().color = new Color(0.65f, 0.65f, 0.65f, 0.8f);
                indexchar--;
                CharBar.transform.GetChild(indexchar).GetComponent<Image>().color = new Color(1f, 0.33f, 0.33f, 0.8f);
            }
        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            print("ChoosingNewChar");
            menu = menuphase.ChoosingNewChar;
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            CharBar.transform.GetChild(indexchar).GetComponent<Image>().color = new Color(0.65f, 0.65f, 0.65f, 0.8f);
            chang = true;

            CharBar.SetActive(false);
            menuManager.returnMainMenu();
            Fcvs.SetActive(false);
        }

    }

    // Update is called once per frame
    void Update()
    {
        switch (menu)
        {
            case menuphase.ChoosingChar:
                {
                    if (!CharBar.activeSelf)
                    {
                        CharBar.SetActive(true);
                        updateCharBar();
                    }
                    choosecharacter();

                    break;
                }
            case menuphase.ChoosingNewChar:
                {
                    if (!SelectableChars.activeSelf)
                    {
                        SelectableChars.SetActive(true);
                        updateSelectableChars();
                    }

                    if (indexchange)
                        ChangeableCharList[index].GetComponentInChildren<TextMeshProUGUI>().color = Color.red;
                    indexchange = false;

                    if (Input.GetKeyDown(KeyCode.DownArrow))
                    {
                        if ((index + 1 % 4) != 0)
                        {
                            if (ChangeableCharList[index + 1].activeSelf)
                            {
                                ChangeableCharList[index].GetComponentInChildren<TextMeshProUGUI>().color = Color.white;
                                index++;
                                indexchange = true;
                            }
                        }
                    }
                    else if (Input.GetKeyDown(KeyCode.UpArrow))
                    {
                        if (index != 0)
                        {
                            if (ChangeableCharList[index - 1].activeSelf)
                            {
                                ChangeableCharList[index].GetComponentInChildren<TextMeshProUGUI>().color = Color.white;
                                index--;
                                indexchange = true;
                            }
                        }
                    }
                    else if (Input.GetKeyDown(KeyCode.RightArrow))
                    {
                        if (index < 4)
                        {
                            if (ChangeableCharList[index + 4].activeSelf)
                            {
                                ChangeableCharList[index].GetComponentInChildren<TextMeshProUGUI>().color = Color.white;
                                index += 4;
                                indexchange = true;
                            }
                        }
                    }
                    else if (Input.GetKeyDown(KeyCode.LeftArrow))
                    {
                        if (index > 3)
                        {
                            if (ChangeableCharList[index - 4].activeSelf)
                            {
                                ChangeableCharList[index].GetComponentInChildren<TextMeshProUGUI>().color = Color.white;
                                index -= 4;
                                indexchange = true;
                            }
                        }
                    }
                    else if (Input.GetKeyDown(KeyCode.Return))
                    {
                        if (auxTeam.Count > 0)
                        {
                            if (indexchar >= ActualTeam.myActualTeam.Length)
                            {
                                auxActualTeam = new GameObject[ActualTeam.myActualTeam.Length + 1];
                                for (int i = 0; i < ActualTeam.myActualTeam.Length; i++)
                                {
                                    auxActualTeam[i] = ActualTeam.myActualTeam[i];
                                }
                                auxActualTeam[ActualTeam.myActualTeam.Length] = auxTeam[index];
                                ActualTeam.myActualTeam = auxActualTeam;
                            }
                            else
                            {
                                //print(indexchar);
                                //print(index);
                                ActualTeam.myActualTeam[indexchar] = auxTeam[index];
                                //ActualTeam.myActualTeam = auxActualTeam;
                            }



                            updateCharBar();
                            updateSelectableChars();
                            ChangeableCharList[index].GetComponentInChildren<TextMeshProUGUI>().color = Color.white;
                            index = 0;
                            indexchange = true;
                            SelectableChars.SetActive(false);
                            menu = menuphase.ChoosingChar;
                        }
                    }
                    else if (Input.GetKeyDown(KeyCode.Escape))
                    {
                        ChangeableCharList[index].GetComponentInChildren<TextMeshProUGUI>().color = Color.white;
                        index = 0;
                        indexchange = true;
                        SelectableChars.SetActive(false);
                        menu = menuphase.ChoosingChar;
                    }
                    break;
                }
        }
    }


    static int SortEquipableObject(so_EquipableObject u1, so_EquipableObject u2)
    {
        return u1.nameObject.CompareTo(u2.nameObject);
    }

}
