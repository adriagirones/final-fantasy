﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class scr_EquipmentManager : MonoBehaviour
{
    public so_Inventory inventory;

    public so_Team AllTeam;
    public GameObject CharChoosed;
    public Image charsprite;
    public GameObject[] txtchar;
    private bool charchanged = false;
    int indexchar = 0;

    private int indexwep = 0;
    private bool weaponchoosed = false;
    private bool equipmentchoosed = false;
    private enum menuphase
    {
        ChoosingChar,
        ChoosingEquip,
        ChangingEquip
    }
    menuphase menu;
    private List<so_Weapon> wepselected = new List<so_Weapon>();

    //public so_GameEvent healingItemEvent;

    public scr_MenuManager menuManager;
    public GameObject Ecvs;
    public GameObject[] txtlist = new GameObject[21];
    private int index = 0;
    private bool indexchange = true;
    public GameObject ItemTray;


    public GameObject page1cvs;
    public GameObject page2cvs;
    public GameObject page3cvs;
    private bool page2;
    private bool page3;

    // Start is called before the first frame update
    void Start()
    {
        ItemTray.SetActive(false);
        menu = menuphase.ChoosingChar;
        //CharChoosed.SetActive(false);
        UpdateCharacter();
    }

    private void OnEnable()
    {
        UpdateCharacter();
    }

    private void updateWeapons(so_Weapon.TypeWeapon tw)
    {
        //deactivate all
        for (int i = 0; i < txtlist.Length; i++)
        {
            txtlist[i].SetActive(false);
        }

        wepselected.Clear();

        //only activate weapons equipable by the character
        foreach (so_Weapon sw in inventory.weapons)
        {
            if (sw.type == tw)
                wepselected.Add(sw);
        }

        //activate the ones we have
        for (int i = 0; i < wepselected.Count; i++)
        {
            txtlist[i].SetActive(true);
            if (wepselected[i].equiped)
            {
                txtlist[i].GetComponent<TextMeshProUGUI>().text = wepselected[i].nameObject + " (E) \n" + wepselected[i].description;
            }
            else
            {
                txtlist[i].GetComponent<TextMeshProUGUI>().text = wepselected[i].nameObject + "\n" + wepselected[i].description;
            }
            if (wepselected[i].sprite != null)
            {
                txtlist[i].transform.GetComponentInChildren<Image>().color = Color.white;
                txtlist[i].transform.GetComponentInChildren<Image>().sprite = wepselected[i].sprite;
            }
            else
                txtlist[i].transform.GetComponentInChildren<Image>().color = Color.clear;
        }
        if (wepselected.Count >= 21)
            page2 = true;
        else
            page2 = false;
        if (wepselected.Count >= 42)
            page3 = true;
        else
            page3 = false;

    }
    private void updateEquipment()
    {
        //deactivate all
        for (int i = 0; i < txtlist.Length; i++)
        {
            txtlist[i].SetActive(false);
        }

        //activate the ones we have
        for (int i = 0; i < inventory.equipableObjects.Count; i++)
        {
            txtlist[i].SetActive(true);
            txtlist[i].GetComponent<TextMeshProUGUI>().text = inventory.equipableObjects[i].nameObject;
            txtlist[i].GetComponent<TextMeshProUGUI>().text += "\n" + inventory.equipableObjects[i].description;
            if (inventory.equipableObjects[i].sprite != null)
            {
                txtlist[i].transform.GetComponentInChildren<Image>().color = Color.white;
                txtlist[i].transform.GetComponentInChildren<Image>().sprite = inventory.equipableObjects[i].sprite;
            }
            else
                txtlist[i].transform.GetComponentInChildren<Image>().color = Color.clear;
        }


        if (inventory.equipableObjects.Count >= 21)
            page2 = true;
        else
            page2 = false;
        if (inventory.equipableObjects.Count >= 42)
            page3 = true;
        else
            page3 = false;

    }

    private void UpdateCharacter()
    {
        if (AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.hp > AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.getGlobalMaxHP())
        {
            AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.hp = AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.getGlobalMaxHP();
        }
        if (AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.mp > AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.getGlobalMaxMP())
        {
            AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.mp = AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.getGlobalMaxMP();
        }

        charsprite.sprite = AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.splashart;
        txtchar[0].GetComponent<TextMeshProUGUI>().text = " HP : " + AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.hp + "/" + AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.getGlobalMaxHP();
        txtchar[2].GetComponent<TextMeshProUGUI>().text = " MP : " + AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.mp + "/" + AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.getGlobalMaxMP();
        txtchar[1].GetComponent<TextMeshProUGUI>().text = " ATK : " + AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.getGlobalAtk();
        txtchar[3].GetComponent<TextMeshProUGUI>().text = " DEF : " + AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.getGlobalDef();
        txtchar[4].GetComponent<TextMeshProUGUI>().text = AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.name;
        if (AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.actualWeapon != null)
        {
            txtchar[5].GetComponent<TextMeshProUGUI>().text = AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.actualWeapon.nameObject + "\n" + AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.actualWeapon.description;
            txtchar[5].GetComponent<RectTransform>().GetComponentInChildren<Image>().color = Color.white;
            txtchar[5].GetComponent<RectTransform>().GetComponentInChildren<Image>().sprite = AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.actualWeapon.sprite;
        }
        else
        {
            txtchar[5].GetComponent<TextMeshProUGUI>().text = "No weapon equiped"; 
            txtchar[5].GetComponent<RectTransform>().GetComponentInChildren<Image>().color = Color.clear;
        }
        if (AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.object1 != null)
        {
            txtchar[6].GetComponent<TextMeshProUGUI>().text = AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.object1.nameObject + "\n" + AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.object1.description;
            txtchar[6].GetComponent<RectTransform>().GetComponentInChildren<Image>().color = Color.white;
            txtchar[6].GetComponent<RectTransform>().GetComponentInChildren<Image>().sprite = AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.object1.sprite;
        }
        else
        {
            txtchar[6].GetComponent<TextMeshProUGUI>().text = "No accessory equiped";
            txtchar[6].GetComponent<RectTransform>().GetComponentInChildren<Image>().color = Color.clear;
        }
        if (AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.object2 != null)
        {
            txtchar[7].GetComponent<TextMeshProUGUI>().text = AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.object2.nameObject + "\n" + AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.object2.description;
            txtchar[7].GetComponent<RectTransform>().GetComponentInChildren<Image>().color = Color.white;
            txtchar[7].GetComponent<RectTransform>().GetComponentInChildren<Image>().sprite = AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.object2.sprite;
        }
        else
        {
            txtchar[7].GetComponent<TextMeshProUGUI>().text = "No accessory equiped";
            txtchar[7].GetComponent<RectTransform>().GetComponentInChildren<Image>().color = Color.clear;
        }
        charchanged = true;
        //print(ActualTeam.myActualTeam[indexchar].GetComponent<scr_Character>().stats.name);
    }

    // Update is called once per frame
    void Update()
    {
        switch (menu)
        {
            case menuphase.ChoosingChar:
                {
                    if (!CharChoosed.activeSelf)
                        CharChoosed.SetActive(true);
                    choosecharacter();

                    break;
                }
            case menuphase.ChoosingEquip:
                {

                    if (indexchange)
                        txtchar[indexwep + 5].GetComponent<TextMeshProUGUI>().color = Color.red;
                    indexchange = false;

                    if (Input.GetKeyDown(KeyCode.DownArrow))
                    {
                        if (indexwep != 2)
                        {
                            txtchar[indexwep + 5].GetComponent<TextMeshProUGUI>().color = Color.white;
                            indexwep++;
                            indexchange = true;
                        }
                    }
                    else if (Input.GetKeyDown(KeyCode.UpArrow))
                    {
                        if (indexwep != 0)
                        {
                            txtchar[indexwep + 5].GetComponent<TextMeshProUGUI>().color = Color.white;
                            indexwep--;
                            indexchange = true;
                        }
                    }
                    else if (Input.GetKeyDown(KeyCode.Return))
                    {
                        txtchar[indexwep + 5].GetComponent<TextMeshProUGUI>().color = Color.red;
                        if (indexwep + 5 > 5)
                            equipmentchoosed = true;
                        else
                            weaponchoosed = true;
                        indexchange = true;
                        menu = menuphase.ChangingEquip;
                    }
                    else if (Input.GetKeyDown(KeyCode.Escape))
                    {
                        txtchar[indexwep + 5].GetComponent<TextMeshProUGUI>().color = Color.white;
                        indexwep = 0;
                        indexchange = true;
                        txtchar[indexwep + 5].GetComponent<TextMeshProUGUI>().color = Color.white;
                        menu = menuphase.ChoosingChar;
                    }

                    break;
                }
            case menuphase.ChangingEquip:
                {
                    if (!ItemTray.activeSelf)
                    {
                        ItemTray.SetActive(true);
                        if (weaponchoosed)
                            updateWeapons(AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.type);
                        else if (equipmentchoosed)
                            updateEquipment();
                    }


                    if (indexchange)
                        txtlist[index].GetComponent<TextMeshProUGUI>().color = Color.red;
                    indexchange = false;

                    if (Input.GetKeyDown(KeyCode.DownArrow))
                    {
                        if ((index + 1 % 7) != 0)
                        {
                            if (txtlist[index + 1].activeSelf)
                            {
                                txtlist[index].GetComponent<TextMeshProUGUI>().color = Color.white;
                                index++;
                                indexchange = true;

                                if (index >= 42 && page3)
                                {
                                    page2cvs.SetActive(false);
                                    page3cvs.SetActive(true);
                                }
                                else if (index >= 21 && page2)
                                {
                                    page1cvs.SetActive(false);
                                    page2cvs.SetActive(true);
                                }
                            }
                        }
                    }
                    else if (Input.GetKeyDown(KeyCode.UpArrow))
                    {
                        if (index != 0)
                        {
                            if (txtlist[index - 1].activeSelf)
                            {
                                txtlist[index].GetComponent<TextMeshProUGUI>().color = Color.white;
                                index--;
                                indexchange = true;

                                if (index < 21 && page2)
                                {
                                    page2cvs.SetActive(false);
                                    page1cvs.SetActive(true);
                                }
                                else if (index < 42 && page3)
                                {
                                    page3cvs.SetActive(false);
                                    page2cvs.SetActive(true);
                                }
                            }
                        }
                    }
                    else if (Input.GetKeyDown(KeyCode.RightArrow))
                    {
                        if (index < 55)
                        {
                            if (txtlist[index + 7].activeSelf)
                            {
                                txtlist[index].GetComponent<TextMeshProUGUI>().color = Color.white;
                                index += 7;
                                indexchange = true;


                                if (index >= 42 && page3)
                                {
                                    page2cvs.SetActive(false);
                                    page3cvs.SetActive(true);
                                }
                                else if (index >= 21 && page2)
                                {
                                    page1cvs.SetActive(false);
                                    page2cvs.SetActive(true);
                                }
                            }
                        }
                    }
                    else if (Input.GetKeyDown(KeyCode.LeftArrow))
                    {
                        if (index > 6)
                        {
                            if (txtlist[index - 7].activeSelf)
                            {
                                txtlist[index].GetComponent<TextMeshProUGUI>().color = Color.white;
                                index -= 7;
                                indexchange = true;


                                if (index < 21 && page2)
                                {
                                    page2cvs.SetActive(false);
                                    page1cvs.SetActive(true);
                                }
                                else if (index < 42 && page3)
                                {
                                    page3cvs.SetActive(false);
                                    page2cvs.SetActive(true);
                                }
                            }

                        }
                    }
                    else if (Input.GetKeyDown(KeyCode.Return))
                    {
                        if (weaponchoosed)
                        {
                            if (wepselected.Count > 0)
                            {
                                if ((so_EquipableObject)wepselected[index] != null && !wepselected[index].equiped)
                                {
                                    wepselected[index].equiped = true;
                                    if (AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.actualWeapon != null)
                                        AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.actualWeapon.equiped = false;
                                    AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.actualWeapon = wepselected[index];
                                    //CHANGE WEP
                                    page1cvs.SetActive(true);
                                    page3cvs.SetActive(false);
                                    page2cvs.SetActive(false);
                                    updateWeapons(AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.type);
                                    UpdateCharacter();
                                    weaponchoosed = false;
                                    equipmentchoosed = false;
                                    txtlist[index].GetComponent<TextMeshProUGUI>().color = Color.white;
                                    index = 0;
                                    txtlist[index].GetComponent<TextMeshProUGUI>().color = Color.red;
                                    ItemTray.SetActive(false);
                                    menu = menuphase.ChoosingEquip;
                                }
                                else
                                {
                                    print("equipedf");
                                }
                            }
                        }
                        else if (equipmentchoosed)
                        {
                            if (inventory.equipableObjects.Count > 0)
                            {
                                if ((so_EquipableObject)inventory.equipableObjects[index] != null && !inventory.equipableObjects[index].equiped)
                                {
                                    if (indexwep + 5 == 6)
                                    {
                                        inventory.equipableObjects[index].equiped = true;
                                        if (AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.object1 != null)
                                            AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.object1.equiped = false;
                                        AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.object1 = inventory.equipableObjects[index];
                                    }
                                    else if (indexwep + 5 == 7)
                                    {
                                        inventory.equipableObjects[index].equiped = true;
                                        if (AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.object2 != null)
                                            AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.object2.equiped = false;
                                        AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.object2 = inventory.equipableObjects[index];
                                    }
                                    page1cvs.SetActive(true);
                                    page3cvs.SetActive(false);
                                    page2cvs.SetActive(false);
                                    updateEquipment();
                                    UpdateCharacter();
                                    weaponchoosed = false;
                                    equipmentchoosed = false;
                                    txtlist[index].GetComponent<TextMeshProUGUI>().color = Color.white;
                                    index = 0;
                                    txtlist[index].GetComponent<TextMeshProUGUI>().color = Color.red;
                                    ItemTray.SetActive(false);
                                    menu = menuphase.ChoosingEquip;
                                    //CHANGE EQUIP
                                }
                                else
                                {
                                    print("equipedf");
                                }
                            }
                        }
                    }
                    else if (Input.GetKeyDown(KeyCode.Escape))
                    {
                        txtlist[index].GetComponent<TextMeshProUGUI>().color = Color.white;
                        index = 0;
                        page1cvs.SetActive(true);
                        page3cvs.SetActive(false);
                        page2cvs.SetActive(false);
                        weaponchoosed = false;
                        equipmentchoosed = false;

                        ItemTray.SetActive(false);
                        menu = menuphase.ChoosingEquip;
                    }
                    break;
                }
        }
    }

    private void choosecharacter()
    {
        //print("hola");
        //indexchar = 0;
        if (!charchanged)
        {
            //charsprite.sprite = ActualTeam.myActualTeam[indexchar].GetComponent<scr_Character>().stats.splashart;
            //txtchar[0].GetComponent<TextMeshProUGUI>().text = " HP : " + ActualTeam.myActualTeam[indexchar].GetComponent<scr_Character>().stats.hp + "/" + ActualTeam.myActualTeam[indexchar].GetComponent<scr_Character>().stats.getGlobalMaxHP();
            //txtchar[2].GetComponent<TextMeshProUGUI>().text = " MP : " + ActualTeam.myActualTeam[indexchar].GetComponent<scr_Character>().stats.mp + "/" + ActualTeam.myActualTeam[indexchar].GetComponent<scr_Character>().stats.getGlobalMaxMP();
            //txtchar[1].GetComponent<TextMeshProUGUI>().text = " ATK : " + ActualTeam.myActualTeam[indexchar].GetComponent<scr_Character>().stats.getGlobalAtk();
            //txtchar[3].GetComponent<TextMeshProUGUI>().text = " DEF : " + ActualTeam.myActualTeam[indexchar].GetComponent<scr_Character>().stats.getGlobalDef();
            //txtchar[4].GetComponent<TextMeshProUGUI>().text = ActualTeam.myActualTeam[indexchar].GetComponent<scr_Character>().stats.name;
            //txtchar[5].GetComponent<TextMeshProUGUI>().text = ActualTeam.myActualTeam[indexchar].GetComponent<scr_Character>().stats.actualWeapon.name + " HP: " + ActualTeam.myActualTeam[indexchar].GetComponent<scr_Character>().stats.actualWeapon.hp + " MP: " + ActualTeam.myActualTeam[indexchar].GetComponent<scr_Character>().stats.actualWeapon.mp + " ATK: " + ActualTeam.myActualTeam[indexchar].GetComponent<scr_Character>().stats.actualWeapon.atk + " DEF: " + ActualTeam.myActualTeam[indexchar].GetComponent<scr_Character>().stats.actualWeapon.def;
            //txtchar[6].GetComponent<TextMeshProUGUI>().text = ActualTeam.myActualTeam[indexchar].GetComponent<scr_Character>().stats.object1.name + " HP: " + ActualTeam.myActualTeam[indexchar].GetComponent<scr_Character>().stats.object1.hp + " MP: " + ActualTeam.myActualTeam[indexchar].GetComponent<scr_Character>().stats.object1.mp + " ATK: " + ActualTeam.myActualTeam[indexchar].GetComponent<scr_Character>().stats.object1.atk + " DEF: " + ActualTeam.myActualTeam[indexchar].GetComponent<scr_Character>().stats.object1.def;
            //txtchar[7].GetComponent<TextMeshProUGUI>().text = ActualTeam.myActualTeam[indexchar].GetComponent<scr_Character>().stats.object2.name + " HP: " + ActualTeam.myActualTeam[indexchar].GetComponent<scr_Character>().stats.object2.hp + " MP: " + ActualTeam.myActualTeam[indexchar].GetComponent<scr_Character>().stats.object2.mp + " ATK: " + ActualTeam.myActualTeam[indexchar].GetComponent<scr_Character>().stats.object2.atk + " DEF: " + ActualTeam.myActualTeam[indexchar].GetComponent<scr_Character>().stats.object2.def;
            UpdateCharacter();
            charchanged = true;
            print(AllTeam.Team[indexchar].GetComponent<scr_Character>().stats.name);
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (indexchar == AllTeam.Team.Count - 1)
                indexchar = 0;
            else
                indexchar++;
            charchanged = false;
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (indexchar == 0)
                indexchar = AllTeam.Team.Count - 1;
            else
                indexchar--;
            charchanged = false;
        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            print("2ndtime");
            charchanged = false;
            menu = menuphase.ChoosingEquip;
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            CharChoosed.SetActive(false);
            menuManager.returnMainMenu();
            Ecvs.SetActive(false);
        }

    }

    static int SortEquipableObject(so_EquipableObject u1, so_EquipableObject u2)
    {
        return u1.nameObject.CompareTo(u2.nameObject);
    }

}
