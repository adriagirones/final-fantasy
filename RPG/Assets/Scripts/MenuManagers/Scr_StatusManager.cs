﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Scr_StatusManager : MonoBehaviour
{
    public so_ActualTeam ActualTeam;
    public GameObject[] chararray = new GameObject[4];
    //public GameObject[,] charlist = new GameObject[4,17];

    public scr_MenuManager menuManager;
    public GameObject Statuscvs;

    // Start is called before the first frame update
    void Start()
    {
        updateChar();
    }

    private void OnEnable()
    {
        updateChar();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Statuscvs.SetActive(false);
            menuManager.returnMainMenu();
        }
    }

    private void updateChar()
    {
        //deactivate the ones we dont have
        for (int i = 0; i < chararray.Length; i++)
        {
            chararray[i].SetActive(false);
        }

        for (int i = 0; i < ActualTeam.myActualTeam.Length; i++)
        {
            chararray[i].SetActive(true);
        }

        for (int i = 0; i < ActualTeam.myActualTeam.Length; i++)
        {
            chararray[i].transform.GetChild(0).GetComponent<Image>().sprite = ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.splashart;
            chararray[i].transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = ActualTeam.myActualTeam[i].GetComponent<scr_Character>().name;
            chararray[i].transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "LV: "+ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.lvl;
            chararray[i].transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = "NEXT LV: " + (ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.xpNextLevel - ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.xp);
            chararray[i].transform.GetChild(4).GetComponent<TextMeshProUGUI>().text = "" + ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.type;

            if (ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.actualSkill() != null)
            {
                chararray[i].transform.GetChild(5).GetComponent<TextMeshProUGUI>().text = "" + ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.actualSkill().nameSkill;
                chararray[i].transform.GetChild(5).GetChild(0).GetComponent<TextMeshProUGUI>().text = ""+ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.actualSkill().description;
            }
            else
            {
                chararray[i].transform.GetChild(5).GetComponent<TextMeshProUGUI>().text = "EMPTY";
                chararray[i].transform.GetChild(5).GetChild(0).GetComponent<TextMeshProUGUI>().text = "";
            }
            if (ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.actualWeapon != null)
            {
                chararray[i].transform.GetChild(6).GetComponent<TextMeshProUGUI>().text = ""+ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.actualWeapon.nameObject;
                chararray[i].transform.GetChild(6).GetComponentInChildren<Image>().color = Color.white;
                chararray[i].transform.GetChild(6).GetComponentInChildren<Image>().sprite = ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.actualWeapon.sprite;
                chararray[i].transform.GetChild(6).GetChild(1).GetComponent<TextMeshProUGUI>().text = "" + ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.actualWeapon.description;
            }
            else
            {
                chararray[i].transform.GetChild(6).GetComponent<TextMeshProUGUI>().text = "EMPTY";
                chararray[i].transform.GetChild(6).GetComponentInChildren<Image>().color = Color.clear;
                chararray[i].transform.GetChild(6).GetChild(1).GetComponent<TextMeshProUGUI>().text="";
            }

            if (ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.object1 != null)
            {
                chararray[i].transform.GetChild(7).GetComponent<TextMeshProUGUI>().text = "" + ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.object1.nameObject;
                chararray[i].transform.GetChild(7).GetComponentInChildren<Image>().color = Color.white;
                chararray[i].transform.GetChild(7).GetComponentInChildren<Image>().sprite = ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.object1.sprite;
                chararray[i].transform.GetChild(7).GetChild(1).GetComponent<TextMeshProUGUI>().text = "" + ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.object1.description;
            }
            else
            {
                chararray[i].transform.GetChild(7).GetComponent<TextMeshProUGUI>().text = "EMPTY";
                chararray[i].transform.GetChild(7).GetComponentInChildren<Image>().color = Color.clear;
                chararray[i].transform.GetChild(7).GetChild(1).GetComponent<TextMeshProUGUI>().text = "";
            }

            if (ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.object2 != null)
            {
                chararray[i].transform.GetChild(8).GetComponent<TextMeshProUGUI>().text = "" + ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.object2.nameObject;
                chararray[i].transform.GetChild(8).GetComponentInChildren<Image>().color = Color.white;
                chararray[i].transform.GetChild(8).GetComponentInChildren<Image>().sprite = ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.object2.sprite;
                chararray[i].transform.GetChild(8).GetChild(1).GetComponent<TextMeshProUGUI>().text = "" + ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.object2.description;
            }
            else
            {
                chararray[i].transform.GetChild(8).GetComponent<TextMeshProUGUI>().text = "EMPTY";
                chararray[i].transform.GetChild(8).GetComponentInChildren<Image>().color = Color.clear;
                chararray[i].transform.GetChild(8).GetChild(1).GetComponent<TextMeshProUGUI>().text = "";
            }

            chararray[i].transform.GetChild(9).GetComponent<TextMeshProUGUI>().text = "HP: " + ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.maxhp;
            chararray[i].transform.GetChild(10).GetComponent<TextMeshProUGUI>().text = "MP: " + ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.maxmp;
            chararray[i].transform.GetChild(11).GetComponent<TextMeshProUGUI>().text = "ATK: " + ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.atk;
            chararray[i].transform.GetChild(12).GetComponent<TextMeshProUGUI>().text = "DEF: " + ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.def;

            chararray[i].transform.GetChild(13).GetComponent<TextMeshProUGUI>().text = "" + ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.hp+"/"+ ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.getGlobalMaxHP();
            chararray[i].transform.GetChild(14).GetComponent<TextMeshProUGUI>().text = "" + ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.mp + "/" + ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.getGlobalMaxMP();
            chararray[i].transform.GetChild(15).GetComponent<TextMeshProUGUI>().text = "" + ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.getGlobalAtk();
            chararray[i].transform.GetChild(16).GetComponent<TextMeshProUGUI>().text = "" + ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.getGlobalDef();

            if (ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.actualSkill() != null)
            {
                chararray[i].transform.GetChild(17).GetComponent<TextMeshProUGUI>().text = "" + ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.actualLimit().nameSkill;
                chararray[i].transform.GetChild(17).GetChild(0).GetComponent<TextMeshProUGUI>().text = "" + ActualTeam.myActualTeam[i].GetComponent<scr_Character>().stats.actualLimit().description;
            }
        }
    }
}
