﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class scr_MagicManager : MonoBehaviour
{
    public so_ActualTeam team;
    public GameObject CharChoosed;
    public Image charsprite;
    public GameObject[] txtchar;
    private bool charchoo = false;
    private bool charchanged = false;

    /*
    enum menuphase
    {
        ChoosingItem,
        ChoosingChar

    }
    */
    int indexchar = 0;

    //public so_GameEvent healingItemEvent;

    public scr_MenuManager menuManager;
    public GameObject Icvs;
    public GameObject[] txtlist = new GameObject[4];
    private int index = 0;
    private bool indexchange = true;



    // Start is called before the first frame update
    void Start()
    {
        CharChoosed.SetActive(false);
        //updateInv();

    }

/*
    private void updateInv(int indexpj)
    {
        //deactivate all
        for (int i = 0; i < txtlist.Length; i++)
        {
            txtlist[i].SetActive(false);
        }

        //activate the ones we have
        for (int i = 0; i < team.myActualTeam[indexpj].GetComponent<so_Character>(); i++)
        {
            
            txtlist[i].SetActive(true);
            txtlist[i].GetComponent<TextMeshProUGUI>().text = inventory.usableItems[i].name + "- num: " + inventory.usableItems[i].quantity;
            if (inventory.usableItems[i].sprite != null)
            {
                txtlist[i].transform.GetChild(0).GetComponent<Image>().color = Color.white;
                txtlist[i].transform.GetChild(0).GetComponent<Image>().sprite = inventory.usableItems[i].sprite;
            }
            else
                txtlist[i].transform.GetChild(0).GetComponent<Image>().color = Color.clear;
        }
    }
*/
    // Update is called once per frame
    void Update()
    {
        //color red to choosed one
        if (!charchoo)
        {

            if (indexchange)
                txtlist[index].GetComponent<TextMeshProUGUI>().color = Color.red;
            indexchange = false;

            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                if (index != 6 && index != 13 && index != 20)
                {
                    txtlist[index].GetComponent<TextMeshProUGUI>().color = Color.white;
                    index++;
                    indexchange = true;
                }
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                if (index != 0 && index != 7 && index != 14)
                {
                    txtlist[index].GetComponent<TextMeshProUGUI>().color = Color.white;
                    index--;
                    indexchange = true;
                }
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                if (index < 14)
                {
                    txtlist[index].GetComponent<TextMeshProUGUI>().color = Color.white;
                    index += 7;
                    indexchange = true;
                }
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                if (index > 6)
                {
                    txtlist[index].GetComponent<TextMeshProUGUI>().color = Color.white;
                    index -= 7;
                    indexchange = true;
                }
            }
            else if (Input.GetKeyDown(KeyCode.Return))
            {
                /*if ((so_UsableItem)inventory.usableItems[index] != null)
                {
                    print("first");
                    charchoo = true;
                    CharChoosed.SetActive(true);
                }*/
            }
            else if (Input.GetKeyDown(KeyCode.Escape))
            {
                menuManager.returnMainMenu();
                Icvs.SetActive(false);

            }
        }
        else
        {
            choosecharacter();
            //charchoo = false;
/*
            if (Input.GetKeyDown(KeyCode.Return))
            {
                if((so_UsableItem)inventory.usableItems[index] != null)
                {
                    if (((so_UsableItem)inventory.usableItems[index]).quantity > 1)
                    {
                        ((so_UsableItem)inventory.usableItems[index]).quantity--;
                        //healingItemEvent.RaiseItem((so_UsableItem)inventory.usableItems[index]);
                        updateInv();
                    }
                    else if (((so_UsableItem)inventory.usableItems[index]).quantity == 1)
                    {
                        ((so_UsableItem)inventory.usableItems[index]).quantity--;
                        //healingItemEvent.RaiseItem((so_UsableItem)inventory.usableItems[index]);
                        inventory.usableItems.RemoveAt(index);
                        inventory.usableItems.Sort(SortUsableItems);
                        updateInv();
                    }
                }
            }*/
        }
        print(index);
    }

    private void choosecharacter()
    {
        //print("hola");
        //indexchar = 0;
        if (!charchanged)
        {
            charsprite.sprite = team.myActualTeam[indexchar].GetComponent<scr_Character>().stats.splashart;
            txtchar[0].GetComponent<TextMeshProUGUI>().text = " HP : " + team.myActualTeam[indexchar].GetComponent<scr_Character>().stats.hp + "/" + team.myActualTeam[indexchar].GetComponent<scr_Character>().stats.maxhp;
            txtchar[2].GetComponent<TextMeshProUGUI>().text = " MP : " + team.myActualTeam[indexchar].GetComponent<scr_Character>().stats.mp + "/" + team.myActualTeam[indexchar].GetComponent<scr_Character>().stats.maxmp;
            txtchar[1].GetComponent<TextMeshProUGUI>().text = " Level : " + team.myActualTeam[indexchar].GetComponent<scr_Character>().stats.lvl;
            txtchar[3].GetComponent<TextMeshProUGUI>().text = " XP : " + team.myActualTeam[indexchar].GetComponent<scr_Character>().stats.xp;
            txtchar[4].GetComponent<TextMeshProUGUI>().text = team.myActualTeam[indexchar].GetComponent<scr_Character>().stats.nameCharacter;
            charchanged = true;
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (indexchar == team.myActualTeam.Length - 1)
                indexchar = 0;
            else
                indexchar++;
            charchanged = false;
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (indexchar == 0)
                indexchar = team.myActualTeam.Length - 1;
            else
                indexchar--;
            charchanged = false;
        }
        //yield return new WaitForSeconds(5f);
        if (Input.GetKeyDown(KeyCode.Return))
        {
            print("2ndtime");
            charchanged = false;
            charchoo = true;
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            charchoo = false;
            CharChoosed.SetActive(false);
        }

    }

    static int SortUsableItems(so_UsableItem u1, so_UsableItem u2)
    {
        return u1.quantity.CompareTo(u2.quantity);
    }

}
