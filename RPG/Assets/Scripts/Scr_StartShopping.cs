﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scr_StartShopping : MonoBehaviour
{
    public GameObject Shop;
    public GameObject textBox;
    public so_Dialogues dialogues;
   // public GameObject textParent;
    //public GameObject textWrite;

    public GameObject shopSO;
    public GameObject PJ;
    public GameObject myCanvas;
    public GameObject menuContext;
    public GameObject menu;

    private bool canBuy;

    private void Start()
    {
        canBuy = false;
    }

    private void Update()
    {
        if (canBuy && !menu.activeSelf )
        {
            if (Input.GetKeyDown(KeyCode.E) && !Shop.activeSelf)
            {
                PJ.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
                myCanvas.SetActive(true);
                menuContext.SetActive(false);
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                PJ.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
                myCanvas.SetActive(false);
                menuContext.SetActive(true);                
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name.Equals("Character"))
        {
            canBuy = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.name.Equals("Character"))
        {
            canBuy = false;
        }
    }

    public void invUsaItems() {
        PJ.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        shopSO.GetComponent<scr_ShopManager>().purchase = scr_ShopManager.typeOfPurchase.POTIONS;
        Shop.SetActive(true);
        shopSO.GetComponent<scr_ShopManager>().closed = true;
        myCanvas.SetActive(false);
    }

    public void invWeapon()
    {
        PJ.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        shopSO.GetComponent<scr_ShopManager>().purchase = scr_ShopManager.typeOfPurchase.WEAPONS;
        Shop.SetActive(true);
        shopSO.GetComponent<scr_ShopManager>().closed = true;
        myCanvas.SetActive(false);
    }

    public void invEquipableObjects()
    {
        PJ.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        shopSO.GetComponent<scr_ShopManager>().purchase = scr_ShopManager.typeOfPurchase.ACCESSORIES;
        Shop.SetActive(true);
        shopSO.GetComponent<scr_ShopManager>().closed = true;
        myCanvas.SetActive(false);
    }

    public void invRecruitment()
    {
        PJ.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        shopSO.GetComponent<scr_ShopManager>().purchase = scr_ShopManager.typeOfPurchase.RECRUITMENT;
        Shop.SetActive(true);
        shopSO.GetComponent<scr_ShopManager>().closed = true;
        myCanvas.SetActive(false);
    }

    /*private void OnTriggerEnter2D(Collider2D collision)
    {

        textParent.SetActive(true);
        textBox.SetActive(true);
        textWrite.SetActive(true);
        textWrite.GetComponent<TMPro.TextMeshProUGUI>().SetText(dialogues.dialogues.dictionary["Pakistani"]);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.name.Equals("Character"))
        {
            textParent.SetActive(false);
            textBox.SetActive(false);
            textWrite.SetActive(false);

        }


    } */
}
