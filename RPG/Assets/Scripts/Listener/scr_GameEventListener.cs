﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

public class scr_GameEventListener : MonoBehaviour
{
    public so_UsableItemEvent Event;

    //listofcharacters

    //public UnityEvent answ;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(so_UsableItem usa, scr_Character cha)
    {
        print("aaa");
        if(usa.GetType() == typeof(so_HealingItem))
        {
            so_HealingItem obj1 = (so_HealingItem)usa;
            print("he llegado aqui");
            cha.stats.RestoreHP(obj1);
            usa.quantity--;
           // EditorUtility.SetDirty(usa);

        }
        else if(usa.GetType() == typeof(so_RecoveryMPItem))
        {
            so_RecoveryMPItem obj1 = (so_RecoveryMPItem)usa;
            cha.stats.RestoreMP(obj1);
            usa.quantity--;
           // EditorUtility.SetDirty(usa);
        }
        else if (usa.GetType() == typeof(so_AttackBuffPotion))
        {
            so_AttackBuffPotion obj1 = (so_AttackBuffPotion)usa;
            print("he llegado aqui");
            cha.BuffAttack(obj1.atk);
            usa.quantity--;
            // EditorUtility.SetDirty(usa);

        }
        else if (usa.GetType() == typeof(so_DefenseBuffPotion))
        {
            so_DefenseBuffPotion obj1 = (so_DefenseBuffPotion)usa;
            cha.BuffDefense(obj1.def);
            usa.quantity--;
            // EditorUtility.SetDirty(usa);
        }
        //Use item on highlighted char

        //answ.Invoke();
    }
}
