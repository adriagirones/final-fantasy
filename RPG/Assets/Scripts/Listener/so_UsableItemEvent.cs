﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_UsableItemEvent : ScriptableObject
{
    private readonly List<scr_GameEventListener> eventListeners = new List<scr_GameEventListener>();

    // Start is called before the first frame update
    public void RaiseItem(so_UsableItem usa, scr_Character cha)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(usa, cha);
    }

    public void RegisterListener(scr_GameEventListener listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(scr_GameEventListener listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}
