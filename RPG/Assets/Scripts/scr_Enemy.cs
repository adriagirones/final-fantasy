﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_Enemy : MonoBehaviour
{
    public so_Enemy stats;

    int maxHpCopy;
    int actualHP;
    int actualAtk;
    int actualDef;
    bool dead;
    int lvl;
    private so_BarHPEvent eventHP;

    // Start is called before the first frame update
    void Start()
    {
        setLvl(lvl);
        if (stats.active)
            this.gameObject.SetActive(true);
        else
            this.gameObject.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void setHPEvent(so_BarHPEvent _event)
    {
        eventHP = _event;
    }

    public void updateHPEvent()
    {
        if (eventHP != null)
        {
            eventHP.RaiseItem(this.gameObject);
        }
    }

    public void setLvl(int _lvl)
    {
        this.lvl = _lvl;
        actualHP = (int)(stats.hpMax * ((lvl * 0.1f) + 1));
        maxHpCopy = (int)(stats.hpMax * ((lvl * 0.1f) + 1));
        actualAtk = (int)(stats.atk * ((lvl * 0.1f) + 1));
        actualDef = (int)(stats.def * ((lvl * 0.1f) + 1));
    }

    public int Attack()
    {
        return actualAtk;
    }

    public void Hurt(int _damage)
    {
        if (_damage - (int)(actualDef * 0.5f) > 0)
        {
            actualHP -= _damage - (int)(actualDef * 0.5f);
            if (actualHP < 0)
            {
                actualHP = 0;
                dead = true;
            }
        }
        if (eventHP != null)
        {
            eventHP.RaiseItem(this.gameObject);
        }
    }

    public bool isDead()
    {
        return dead;
    }

    public void debuffAtk(int _debuff)
    {

        actualAtk -= _debuff;
    }

    public void debuffDef(int _debuff)
    {
        actualDef -= _debuff;
    }

    public int getActualHP()
    {
        return actualHP;
    }

    public int getActualHPMax()
    {
        return maxHpCopy;
    }

    public int getActualAtk()
    {
        return actualAtk;
    }

    public int getActualDef()
    {
        return actualDef;
    }

    public int getXP()
    {
        return stats.xp * lvl;
    }

    public int getActualLvl()
    {
        return lvl;
    }

}
